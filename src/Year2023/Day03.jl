module Day03

function part01(path)
    lines = readlines(path)

    rows = length(lines)
    columns = maximum(length, lines)
    chars = Matrix{Char}(undef, rows, columns)
    for (i, row) in enumerate(lines)
        for (j, char) in enumerate(row)
            chars[i, j] = char
        end
    end

    adjacencies = fill(false, rows, columns)
    for j0 = 1:columns, i0 = 1:rows
        for δj = -1:1, δi = -1:1
            i = i0 + δi
            j = j0 + δj
            if isdigit(chars[i0, j0]) &&
               checkbounds(Bool, chars, i, j) &&
               !isdigit(chars[i, j]) &&
               chars[i, j] != '.'
                adjacencies[i0, j0] = true
            end
        end
    end

    sum = 0
    for i = 1:rows
        j = 1
        while j <= columns
            buffer = IOBuffer()
            adjacent = false
            while j <= columns && isdigit(chars[i, j])
                write(buffer, chars[i, j])
                adjacent |= adjacencies[i, j]
                j += 1
            end
            number = String(take!(buffer))
            if adjacent && !isempty(number)
                sum += parse(Int, number)
            end
            j += 1
        end
    end

    sum
end

function part02(path)
    lines = readlines(path)

    rows = length(lines)
    columns = maximum(length, lines)

    matrix = Matrix{Char}(undef, rows, columns)
    for (i, line) in enumerate(lines)
        for (j, char) in enumerate(line)
            matrix[i, j] = char
        end
    end

    sum = 0
    for i = 1:rows, j = 1:columns
        if matrix[i, j] == '*'
            visited = Set{Tuple{Int,Int}}([(i, j)])
            numbers = Int[]
            for δj = -1:1, δi = -1:1
                r = i + δi
                c = j + δj

                if isdigit(matrix[r, c]) && !((r, c) in visited)
                    push!(visited, (r, c))
                    number = [(c, matrix[r, c])]

                    ic = c - 1
                    while ic >= 1 && isdigit(matrix[r, ic]) && !((r, ic) in visited)
                        push!(number, (ic, matrix[r, ic]))
                        push!(visited, (r, ic))
                        ic -= 1
                    end

                    ic = c + 1
                    while ic <= columns && isdigit(matrix[r, ic]) && !((r, ic) in visited)
                        push!(number, (ic, matrix[r, ic]))
                        push!(visited, (r, ic))
                        ic += 1
                    end

                    sort!(number; by = x -> x[1])
                    push!(numbers, parse(Int, String(map(x -> x[2], number))))
                end
            end

            if length(numbers) == 2
                sum += prod(numbers)
            end
        end
    end

    sum
end

end
