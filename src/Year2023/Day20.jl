module Day20

using DataStructures
using Graphs

@enum Strength HIGH LOW

struct Pulse
    strength::Strength
    destination::String
end

abstract type Module end

struct System
    connections::Graph{Int}
    labels::Dict{String,Int}
    modules::Vector{Module}
    pulses::Queue{Pulse}
end

function load(path)
    lines = readlines(path)

    connections = Dict{String,Vector{String}}()
    for line in lines
        from, tos = split(line, "->")
        from = strip(from)
        to = strip.(split(tos, ','))
        connections[from] = to
    end

    connections
end

end
