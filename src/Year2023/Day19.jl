module Day19

@enum Operation LT GT

struct Rule
    component::Symbol
    operation::Operation
    value::Int
    then::String
end

struct Workflow
    rules::Vector{Rule}
    otherwise::String
end

struct Part{I}
    x::I
    m::I
    a::I
    s::I
end

Part(; x, m, a, s) = Part(x, m, a, s)

function load(path)
    open(path) do f
        workflows = Dict{String,Workflow}()
        while (line = readline(f)) != ""
            buffer = IOBuffer(line)
            label = String(readuntil(buffer, '{'))
            rules = Rule[]
            while true
                component = read(buffer, Char)
                operation = read(buffer, Char)

                if operation != '>' && operation != '<'
                    otherwise = if operation != '}'
                        string(component, operation, readuntil(buffer, '}'))
                    else
                        string(component)
                    end

                    @assert !haskey(workflows, label)
                    workflows[label] = Workflow(rules, otherwise)
                    break
                end

                @assert component in ['x', 'm', 'a', 's']
                @assert operation in ['<', '>']

                value = parse(Int, readuntil(buffer, ':'))
                then = readuntil(buffer, ',')
                push!(rules, Rule(Symbol(component), if operation == '<'
                    LT
                else
                    GT
                end, value, then))
            end
        end

        parts = Part[]
        while !eof(f)
            line = readline(f)
            m = match(r"^\{x=(\d+),m=(\d+),a=(\d+),s=(\d+)\}$", line)
            values = parse.(Int, m.captures)
            push!(parts, Part(values...))
        end

        workflows, parts
    end
end

function run(workflow::Workflow, part::Part{Int})
    for rule in workflow.rules
        component = getproperty(part, rule.component)
        if (rule.operation == GT && component > rule.value) ||
           (rule.operation == LT && component < rule.value)
            return rule.then
        end
    end

    workflow.otherwise
end

function run(workflows::Dict{String,Workflow}, part::Part{Int})
    next = "in"
    while next != "R" && next != "A"
        next = run(workflows[next], part)
    end
    next == "A"
end

function part01(path)
    workflows, parts = load(path)
    accepted = filter(p -> run(workflows, p), parts)
    sum(p -> p.x + p.m + p.a + p.s, accepted)
end

function run(workflow::Workflow, parts::Part{UnitRange{Int}})
    labels = Tuple{Part{UnitRange{Int}},String}[]
    for rule in workflow.rules
        current = getproperty(parts, rule.component)

        captured, onwards = if rule.operation == GT
            rule.value+1:last(current), first(current):rule.value
        elseif rule.operation == LT
            first(current):rule.value-1, rule.value:last(current)
        end

        d = Dict(p => getproperty(parts, p) for p in fieldnames(typeof(parts)))

        if !isempty(captured)
            d[rule.component] = captured
            push!(labels, (Part(; d...), rule.then))
        end

        if !isempty(onwards)
            d[rule.component] = onwards
            parts = Part(; d...)
        else
            return labels
        end
    end

    push!(labels, (parts, workflow.otherwise))

    labels
end

function part02(path)
    workflows, _ = load(path)

    accepted = Part{UnitRange{Int}}[]
    rejected = Part{UnitRange{Int}}[]
    parts = [(Part(1:4000, 1:4000, 1:4000, 1:4000), "in")]
    while !isempty(parts)
        (part, label) = pop!(parts)
        next = run(workflows[label], part)
        for (part, label) in next
            if label == "A"
                push!(accepted, part)
            elseif label == "R"
                push!(rejected, part)
            else
                push!(parts, (part, label))
            end
        end
    end

    sum(accepted) do part
        length(part.x) * length(part.m) * length(part.a) * length(part.s)
    end
end

end
