module Day15

function hash(s)
    value = UInt(0)
    for c in s
        value += Int8(c)
        value *= 17
        value %= 256
    end

    UInt8(value)
end

function part01(path)
    inputs = split(readchomp(path), ',')
    hashes = hash.(inputs)
    Int(sum(hashes))
end

abstract type Operation end

struct Remove <: Operation end
struct Add <: Operation
    focus::Int
end

function part02(path)
    inputs = split(readchomp(path), ',')
    decoded = map(inputs) do i
        m = match(r"(.+)(-|=\d)", i)
        label, op = m.captures
        operation = if op[1] == '='
            Add(parse(Int, op[2]))
        elseif op[1] == '-'
            Remove()
        else
            error("$op is not a recognised operation.")
        end

        (String(label), operation)
    end

    boxes = [Tuple{String,Int}[] for _ = 1:256]
    for (label, operation) in decoded
        box = hash(label) + 1
        position = findfirst(lens -> lens[1] == label, boxes[box])
        if operation isa Add
            if position === nothing
                push!(boxes[box], (label, operation.focus))
            else
                boxes[box][position] = (label, operation.focus)
            end
        elseif operation isa Remove
            if position !== nothing
                deleteat!(boxes[box], position)
            end
        else
            error("WTF is $operation?!?!?")
        end
    end

    total = 0
    for (boxi, box) in enumerate(boxes)
        for (lensi, lens) in enumerate(box)
            _, focus = lens
            total += boxi * lensi * focus
        end
    end

    total
end

end
