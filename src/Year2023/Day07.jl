module Day07

export part01

module Part01
const CARDS = Dict{Char,UInt8}(
    card => code for (code, card) in enumerate(
        reverse(['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2']),
    )
)

struct Cards
    cards::Vector{UInt8}
    counts::Dict{UInt8,Int}

    function Cards(str::AbstractString)
        cards = map(c -> CARDS[c], collect(str))
        counts = Dict(card => 0 for card in values(CARDS))
        for card in cards
            counts[card] += 1
        end
        new(cards, counts)
    end
end

@enum Type HighCard OnePair TwoPair ThreeOfAKind FullHouse FourOfAKind FiveOfAKind

function type(card)
    counts = sort(collect(values(card.counts)); rev = true)
    if counts[1] == 5
        FiveOfAKind
    elseif counts[1] == 4
        FourOfAKind
    elseif counts[1] == 3 && counts[2] == 2
        FullHouse
    elseif counts[1] == 3
        ThreeOfAKind
    elseif counts[1] == 2 && counts[2] == 2
        TwoPair
    elseif counts[1] == 2
        OnePair
    elseif counts[1] == 1
        HighCard
    else
        error("Invalid counts!")
    end
end

function weaker(self, other)
    stype = type(self)
    otype = type(other)

    stype < otype || (stype == otype && self.cards < other.cards)
end

function part01(path)
    lines = readlines(path)
    card_bids = map(lines) do line
        cards_str, bid_str = split(line)
        bid = parse(Int, bid_str)
        cards = Cards(cards_str)
        cards, bid
    end

    sort!(card_bids, by = first, lt = weaker)
    bids = last.(card_bids)
    sum(i * b for (i, b) in enumerate(bids))
end
end

using .Part01: part01

module Part02
const CARDS = Dict{Char,UInt8}(
    card => code for (code, card) in enumerate(
        reverse(['A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J']),
    )
)

struct Cards
    cards::Vector{UInt8}
    strongest::Vector{UInt8}
    counts::Dict{UInt8,Int}
end

function Cards(str::AbstractString)
    actual_cards = map(c -> CARDS[c], collect(str))

    possibilities = Cards[]
    for swap in keys(CARDS)
        cards = map(collect(str)) do c
            if c == 'J'
                CARDS[swap]
            else
                CARDS[c]
            end
        end

        counts = Dict(card => 0 for card in values(CARDS))

        for card in cards
            counts[card] += 1
        end

        push!(possibilities, Cards(cards, cards, counts))
    end

    sort!(possibilities; lt = weaker, rev = true)
    strongest = first(possibilities)

    Cards(actual_cards, strongest.cards, strongest.counts)
end

@enum Type HighCard OnePair TwoPair ThreeOfAKind FullHouse FourOfAKind FiveOfAKind

function type(card)
    counts = sort(collect(values(card.counts)); rev = true)
    if counts[1] == 5
        FiveOfAKind
    elseif counts[1] == 4
        FourOfAKind
    elseif counts[1] == 3 && counts[2] == 2
        FullHouse
    elseif counts[1] == 3
        ThreeOfAKind
    elseif counts[1] == 2 && counts[2] == 2
        TwoPair
    elseif counts[1] == 2
        OnePair
    elseif counts[1] == 1
        HighCard
    else
        error("Invalid counts!")
    end
end

function weaker(self, other)
    stype = type(self)
    otype = type(other)

    stype < otype || (stype == otype && self.cards < other.cards)
end

function part02(path)
    lines = readlines(path)
    card_bids = map(lines) do line
        cards_str, bid_str = split(line)
        bid = parse(Int, bid_str)
        cards = Cards(cards_str)
        cards, bid
    end

    sort!(card_bids, by = first, lt = weaker)
    bids = last.(card_bids)
    sum(i * b for (i, b) in enumerate(bids))
end
end

using .Part02: part02

end
