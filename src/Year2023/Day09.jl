module Day09

function extrapolate(history, operator, selector)
    boundaries = Int[]
    list = history
    while !all(==(0), list)
        push!(boundaries, selector(list))
        list = diff(list)
    end

    foldr(operator, boundaries)
end

function part01(path)
    lines = readlines(path)
    histories = map(l -> parse.(Int, l), split.(lines))
    extrapolated = extrapolate.(histories, +, last)
    sum(extrapolated)
end

function part02(path)
    lines = readlines(path)
    histories = map(l -> parse.(Int, l), split.(lines))
    extrapolated = extrapolate.(histories, -, first)
    sum(extrapolated)
end

end
