module Day12

@enum State WORKING BROKEN UNKNOWN

Base.show(io::IO, s::State) = print(io, if s == WORKING
    '.'
elseif s == BROKEN
    '#'
elseif s == UNKNOWN
    '?'
else
    error("??!")
end)

function Base.show(io::IO, v::Vector{State})
    for s in v
        show(io, s)
    end
end

module Slow

using ..Day12: State, BROKEN, WORKING, UNKNOWN

function isvalid(solution::Vector{State}, groups::Vector{Int})
    i = 1
    group = 1
    final = something(findfirst(==(UNKNOWN), solution), length(solution) + 1) - 1
    while i <= final
        if solution[i] == UNKNOWN
            error("???")
        elseif solution[i] == BROKEN
            if !checkbounds(Bool, groups, group)
                return false
            end

            group_end = i + groups[group]
            if checkbounds(Bool, solution, group_end - 1) &&
               all(!=(WORKING), solution[i:group_end-1]) &&
               (!checkbounds(Bool, solution, group_end) || solution[group_end] != BROKEN)
                group += 1
                i = group_end
            else
                return false
            end
        end
        i += 1
    end

    true
end

function possibilities(template::Vector{State}, groups::Vector{Int})
    solutions = [(template, 1)]
    next = empty(solutions)
    for group in groups
        while !isempty(solutions)
            t, start = pop!(solutions)
            for i = start:length(t)
                group_end = i + group

                if checkbounds(Bool, t, group_end - 1) &&
                   all(!=(WORKING), t[i:group_end-1]) &&
                   (!checkbounds(Bool, t, group_end) || t[group_end] != BROKEN)
                    t2 = copy(t)

                    t2[i:group_end-1] .= BROKEN
                    if checkbounds(Bool, t2, group_end)
                        t2[group_end] = WORKING
                    end
                    t2[first(axes(t2)).<i.&&t2.==UNKNOWN] .= WORKING

                    if isvalid(t2, groups)
                        push!(next, (t2, group_end + 1))
                    end
                end
            end
        end

        temp = solutions
        solutions = next
        next = temp
    end

    solutions = map(solutions) do sol
        s, _ = sol
        s[s.==UNKNOWN] .= WORKING
        s
    end

    filter(s -> isvalid(s, groups), solutions)
end

end

using .Slow

function load(path, folds)
    lines = readlines(path)
    map(lines) do l
        template_str, groups_str = split(l)
        template_str = join(repeat([template_str], folds), '?')
        template = map(collect(template_str)) do c
            if c == '.'
                WORKING
            elseif c == '#'
                BROKEN
            elseif c == '?'
                UNKNOWN
            else
                error("Character '$c' not recognised!")
            end
        end
        groups = parse.(Int, repeat(split(groups_str, ','), folds))

        template, groups
    end
end

function part01(path)
    rows = load(path, 1)
    map(rows) do r
        t, g = r
        p = Slow.possibilities(t, g)
        length(p)
    end |> sum
end

module Fast

using Memoize

using ..Day12: State, BROKEN, UNKNOWN, WORKING

@memoize Dict function count_possibilities(template::Vector{State}, groups::Vector{Int})
    if isempty(template)
        return Int(isempty(groups))
    end
    if isempty(groups)
        return Int(!any(==(BROKEN), template))
    end
    if length(template) < sum(groups) + length(groups) - 1
        return 0
    end

    if first(template) == WORKING
        return count_possibilities(template[2:end], groups)
    elseif first(template) == BROKEN
        this, others... = groups

        if length(template) < this || any(==(WORKING), template[1:this])
            return 0
        end

        if length(template) >= this + 1 && template[this+1] == BROKEN
            return 0
        end

        return count_possibilities(template[this+2:end], others)
    elseif first(template) == UNKNOWN
        return count_possibilities([WORKING, template[2:end]...], groups) +
               count_possibilities([BROKEN, template[2:end]...], groups)
    else
        error("??!?!?")
    end
end

end

using .Fast

function part02(path)
    rows = load(path, 5)
    map(rows) do r
        t, g = r
        Fast.count_possibilities(t, g)
    end |> sum
end

end
