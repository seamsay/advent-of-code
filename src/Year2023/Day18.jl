module Day18

using LinearAlgebra

function load(path)
    lines = readlines(path)
    instructions = map(lines) do line
        m = match(r"^([UDRL])\s+(\d+)\s+\(#([0-9a-f]{5})([0-3])\)$", line)

        p1_direction = if m.captures[1] == "U"
            (-1, 0)
        elseif m.captures[1] == "D"
            (1, 0)
        elseif m.captures[1] == "R"
            (0, 1)
        elseif m.captures[1] == "L"
            (0, -1)
        else
            error("RegEx should have prevented this.")
        end

        p1_distance = parse(Int, m.captures[2])
        p2_distance = parse(Int, m.captures[3], base = 16)

        p2_direction = if m.captures[4] == "0"
            (0, 1)
        elseif m.captures[4] == "1"
            (1, 0)
        elseif m.captures[4] == "2"
            (0, -1)
        elseif m.captures[4] == "3"
            (-1, 0)
        else
            error("RegEx should have prevented this.")
        end

        ((p1_direction, p1_distance), (p2_direction, p2_distance))
    end

    instructions
end

function run(instructions)
    boundary = Tuple{Int,Int}[(1, 1)]
    n = 0
    position = (1, 1)
    for (direction, distance) in instructions
        n += distance
        position = position .+ distance .* direction
        push!(boundary, position)
    end

    @assert first(boundary) == last(boundary)
    total = 0
    for i = 2:length(boundary)
        m = stack(boundary[i-1:i])
        total += round(Int, det(m))
    end
    total = div(abs(total) + n, 2) + 1

    total
end

function part01(path)
    instructions = load(path)
    run(first.(instructions))
end

function part02(path)
    instructions = load(path)
    run(last.(instructions))
end

end
