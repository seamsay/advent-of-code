module Day08

using SparseArrays

const LEFT = 1
const RIGHT = 2

index(label) = 1 + sum((collect(label) .- 'A') .* 26 .^ [2, 1, 0])
function label(index)
    l = ['A', 'A', 'A']
    n = index - 1
    for i in reverse(1:3)
        n, d = divrem(n, 26)
        l[i] += d
    end

    @assert n == 0
    String(l)
end

struct Graph
    edges::SparseMatrixCSC{Int}

    Graph() = new(spzeros(Int, index("ZZZ"), index("ZZZ")))
end

function parse_input(f)
    steps = collect(readline(f))
    directions = @. (LEFT * (steps == 'L')) + (RIGHT * (steps == 'R'))

    @assert isempty(readline(f))

    graph = (spzeros(Int8, 26^3, 26^3), spzeros(Int8, 26^3, 26^3))
    while !eof(f)
        l = readline(f)

        from = index(l[1:3])
        left = index(l[8:10])
        right = index(l[13:15])

        graph[LEFT][left, from] = 1
        graph[RIGHT][right, from] = 1
    end

    graph, directions
end

function path_length(condition, graph, directions, start)
    v = spzeros(Int8, 26^3)
    v[start] = 1

    i = 0
    for direction in Iterators.cycle(directions)
        v = graph[direction] * v
        i += 1
        condition(v) && break
    end

    i
end

function part01(path)
    graph, directions = open(parse_input, path)
    path_length(v -> last(v) != 0, graph, directions, 1)
end

function part02(path)
    graph, directions = open(parse_input, path)

    i1s = Int[]
    for (_, from, value) in zip(findnz(graph[1])...)
        value == 0 && continue

        if from % 26 == 1  # Last letter of the label is 'A'.
            push!(i1s, from)
        end
    end

    i2s = Int[]
    for (_, from, value) in zip(findnz(graph[2])...)
        value == 0 && continue

        if from % 26 == 1  # Last letter of the label is 'A'.
            push!(i2s, from)
        end
    end

    @assert i1s == i2s

    # Stupid unstated assumptions are stupid...
    lcm(
        path_length.(
            v -> only(first(findnz(v))) % 26 == 0,
            Ref(graph),
            Ref(directions),
            i1s,
        ),
    )
end

end
