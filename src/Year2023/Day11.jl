module Day11

function part01(path)
    lines = readlines(path)
    preexpanded = permutedims(stack(map(l -> collect(l) .== '#', lines)))
    empty_columns = findall(!any, eachcol(preexpanded))
    empty_rows = findall(!any, eachrow(preexpanded))

    cluster = fill(
        false,
        size(preexpanded, 1) + length(empty_rows),
        size(preexpanded, 2) + length(empty_columns),
    )
    c = pc = 1
    while pc <= size(preexpanded, 2)
        r = pr = 1
        while pr <= size(preexpanded, 1)
            # I know you could just set `cluster` to `preexpanded`, but I think this is clearer.
            if preexpanded[pr, pc]
                cluster[r, c] = true
            end

            if pr in empty_rows
                @assert !preexpanded[pr, pc]
                @assert !cluster[r, c]
                r += 1
            end

            r += 1
            pr += 1
        end

        if pc in empty_columns
            @assert !any(preexpanded[:, pc])
            @assert !any(cluster[:, c])
            c += 1
        end

        c += 1
        pc += 1
    end

    galaxies = findall(cluster)

    total = 0
    for i in eachindex(galaxies)
        for j in Iterators.drop(eachindex(galaxies), i)
            g1 = Tuple(galaxies[i])
            g2 = Tuple(galaxies[j])
            distance = sum(@. abs(g2 - g1))
            total += distance
        end
    end

    total
end

function shortest(m, g1, g2, empty_rows, empty_columns)
    er = count(min(g1[1], g2[1]) .< empty_rows .< max(g1[1], g2[1]))
    ec = count(min(g1[2], g2[2]) .< empty_columns .< max(g1[2], g2[2]))

    e = (er, ec)

    sum(@. abs(g2 - g1) + (m - 1) * e)
end

function part02(path, m = 1_000_000)
    lines = readlines(path)
    cluster = permutedims(stack(map(l -> collect(l) .== '#', lines)))
    empty_columns = findall(!any, eachcol(cluster))
    empty_rows = findall(!any, eachrow(cluster))

    galaxies = findall(cluster)

    total = 0
    for i in eachindex(galaxies)
        for j in Iterators.drop(eachindex(galaxies), i)
            g1 = Tuple(galaxies[i])
            g2 = Tuple(galaxies[j])

            total += shortest(m, g1, g2, empty_rows, empty_columns)
        end
    end

    total
end

end
