module Day01

function getnth(pred, haystack, n)
    # To be unicode safe, we don't use `findfirst`/`findlast`.
    n, iterator = if n > 0
        n, haystack
    elseif n < 0
        abs(n), Iterators.reverse(haystack)
    else
        error("Can't find the 0th item of something!")
    end

    i = zero(n)
    for item in iterator
        pred(item) && (i += 1)
        i == n && return item
    end

    error("Item not found!")
end

function part01(path)
    lines = readlines(path)
    fs = getnth.(isdigit, lines, 1)
    ls = getnth.(isdigit, lines, -1)
    numbers = string.(fs, ls)
    parse.(Int, numbers) |> sum
end

function digits(haystack)
    table = Dict(
        "0" => 0,
        "zero" => 0,
        "1" => 1,
        "one" => 1,
        "2" => 2,
        "two" => 2,
        "3" => 3,
        "three" => 3,
        "4" => 4,
        "four" => 4,
        "5" => 5,
        "five" => 5,
        "6" => 6,
        "six" => 6,
        "7" => 7,
        "seven" => 7,
        "8" => 8,
        "eight" => 8,
        "9" => 9,
        "nine" => 9,
    )

    digits = valtype(table)[]
    nh = ncodeunits(haystack)
    for i = 1:nh
        for (needle, digit) in table
            nn = ncodeunits(needle)
            if i + nn - 1 <= nh && haystack[i:i+nn-1] == needle
                push!(digits, digit)
            end
        end
    end

    digits
end

function part02(path)
    lines = readlines(path)
    ds = digits.(lines)
    fs = first.(ds)
    ls = last.(ds)
    sum(10 .* fs .+ ls)
end

end
