module Day16

load(path) = permutedims(stack(readlines(path)))

function simulate(grid, initial_position, initial_velocity)
    fronts = Set{NTuple{2,NTuple{2,Int}}}()
    initial_row, initial_column = initial_position
    mirror = grid[initial_row, initial_column]

    if mirror == '-' && initial_velocity[1] != 0
        push!(fronts, (initial_position, (0, 1)))
        push!(fronts, (initial_position, (0, -1)))
    elseif mirror == '|' && initial_velocity[2] != 0
        push!(fronts, (initial_position, (1, 0)))
        push!(fronts, (initial_position, (-1, 0)))
    elseif mirror == '/'
        ivr, ivc = initial_velocity
        initial_velocity = (-ivc, -ivr)
        push!(fronts, (initial_position, initial_velocity))
    elseif mirror == '\\'
        ivr, ivc = initial_velocity
        initial_velocity = (ivc, ivr)
        push!(fronts, (initial_position, initial_velocity))
    else
        push!(fronts, (initial_position, initial_velocity))
    end

    beams = empty(fronts)
    while !isempty(setdiff(fronts, beams))
        new_fronts = empty(fronts)

        for front in fronts
            push!(beams, front)

            position, velocity = front
            @assert (abs(velocity[1]) == 1 && velocity[2] == 0) ||
                    (velocity[1] == 0 && abs(velocity[2]) == 1)
            position = position .+ velocity

            row, column = position
            if !checkbounds(Bool, grid, row, column)
                continue
            end
            mirror = grid[row, column]

            if mirror == '-' && velocity[1] != 0
                push!(new_fronts, (position, (0, 1)))
                push!(new_fronts, (position, (0, -1)))
            elseif mirror == '|' && velocity[2] != 0
                push!(new_fronts, (position, (1, 0)))
                push!(new_fronts, (position, (-1, 0)))
            elseif mirror == '/'
                vr, vc = velocity
                velocity = (-vc, -vr)
                push!(new_fronts, (position, velocity))
            elseif mirror == '\\'
                vr, vc = velocity
                velocity = (vc, vr)
                push!(new_fronts, (position, velocity))
            else
                push!(new_fronts, (position, velocity))
            end
        end

        fronts = new_fronts
    end

    positions = Set(p for (p, _) in beams)
    length(positions)
end

function part01(path)
    grid = load(path)
    simulate(grid, (1, 1), (0, 1))
end

function part02(path)
    grid = load(path)

    starts = NTuple{2,NTuple{2,Int}}[]
    for row = 1:size(grid, 1)
        push!(starts, ((row, 1), (0, 1)))
        push!(starts, ((row, size(grid, 2)), (0, -1)))
    end
    for column = 1:size(grid, 2)
        push!(starts, ((1, column), (1, 0)))
        push!(starts, ((size(grid, 1), column), (-1, 0)))
    end

    energised = map(starts) do pv
        initial_position, initial_velocity = pv
        simulate(grid, initial_position, initial_velocity)
    end

    maximum(energised)
end

end
