module Day04

struct Card
    id::Int
    winners::Set{Int}
    owned::Set{Int}
end

function Base.parse(::Type{Card}, string)
    header, card = split(string, ':')

    header_match = match(r"^Card\s+(\d+)", header)
    id = parse(Int, only(header_match.captures))

    win_side, own_side = split(card, '|')
    winners = Set(parse.(Int, split(win_side)))
    owned = Set(parse.(Int, split(own_side)))

    Card(id, winners, owned)
end

function part01(path)
    lines = readlines(path)
    cards = parse.(Card, lines)
    matching = map(c -> intersect(c.winners, c.owned), cards)
    nmatches = filter(>(0), length.(matching))
    sum(2 .^ (nmatches .- 1))
end

function part02(path)
    lines = readlines(path)
    cards = parse.(Card, lines)
    matching = map(c -> intersect(c.winners, c.owned), cards)
    nmatches = length.(matching)

    @assert length(cards) == length(nmatches)

    # The `reverse` calls aren't necessary for correctness, but help with debugging.
    stack = reverse(cards)
    won = 0
    while !isempty(stack)
        card = pop!(stack)
        won += 1

        matches = length(intersect(card.winners, card.owned))
        copies = let i = card.id, n = matches
            cards[i+1:i+n]
        end

        append!(stack, reverse(copies))
    end

    won
end

end
