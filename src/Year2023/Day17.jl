module Day17

using DataStructures

@enum Direction UP DOWN LEFT RIGHT START

function load(path)
    lines = readlines(path)
    numbers = map(l -> parse.(Int, collect(l)), lines)
    permutedims(stack(numbers))
end

function run(path, range)
    grid = load(path)

    queue = PriorityQueue{Tuple{Complex{Int},Complex{Int}},Int}(
        (1 + 1im, 1 + 0im) => 0,
        (1 + 1im, 1im) => 0,
    )
    seen = Set{Tuple{Complex{Int},Complex{Int}}}()
    while !isempty(queue)
        (p, d), v = dequeue_pair!(queue)
        if (p, d) in seen
            continue
        end
        push!(seen, (p, d))

        r, c = reim(p)
        if (r, c) == size(grid)
            return v
        end

        for nd in (im * d, -im * d)
            for i in range
                np = p + i * nd
                nr, nc = reim(np)

                if checkbounds(Bool, grid, nr, nc)
                    t =
                        v + sum(
                            grid[
                                min(nr, r + 1):max(nr, r - 1),
                                min(nc, c + 1):max(nc, c - 1),
                            ],
                        )

                    key = (np, nd)
                    if haskey(queue, key)
                        queue[key] = min(queue[key], t)
                    else
                        enqueue!(queue, key, t)
                    end
                end
            end
        end
    end
end

part01(path) = run(path, 1:3)
part02(path) = run(path, 4:10)

end
