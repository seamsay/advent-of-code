module Day02

struct Draw
    red::Int
    green::Int
    blue::Int
end

struct Game
    id::Int
    draws::Vector{Draw}
end

function Base.parse(::Type{Game}, str)
    header, game = split(str, ':')
    game = strip(game)

    i = parse(Int, match(r"^Game (\d+)$", header).captures[1])

    draws = Draw[]
    for play in split(game, ';')
        play = strip(play)
        red = green = blue = 0
        for colour in split(play, ',')
            count, colour = split(strip(colour))
            n = parse(Int, count)
            if colour == "red"
                red = n
            elseif colour == "green"
                green = n
            elseif colour == "blue"
                blue = n
            else
                error("Unrecognised colour: $colour")
            end
        end
        push!(draws, Draw(red, green, blue))
    end

    Game(i, draws)
end

ispossible(total, draw::Draw) =
    draw.red <= total.red && draw.green <= total.green && draw.blue <= total.blue
ispossible(total, game::Game) = all(d -> ispossible(total, d), game.draws)

function part01(path)
    lines = readlines(path)
    games = parse.(Game, lines)
    possible = filter(g -> ispossible(Draw(12, 13, 14), g), games)
    sum(g -> g.id, possible)
end

Base.max(l::Draw, r::Draw) =
    Draw(max(l.red, r.red), max(l.green, r.green), max(l.blue, r.blue))
Base.maximum(game::Game) = reduce(max, game.draws; init = Draw(0, 0, 0))
power(draw::Draw) = draw.red * draw.green * draw.blue

function part02(path)
    lines = readlines(path)
    games = parse.(Game, lines)
    maximums = maximum.(games)
    powers = power.(maximums)
    sum(powers)
end

end
