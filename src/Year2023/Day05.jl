module Day05

struct RangeDict
    srcs::Vector{UnitRange{Int}}
    dsts::Vector{UnitRange{Int}}

    function RangeDict(srcs, dsts)
        @assert length(srcs) == length(dsts)
        @assert all(Iterators.zip(srcs, dsts)) do x
            s, d = x
            length(s) == length(d)
        end

        perm = sortperm(srcs)
        new(srcs[perm], dsts[perm])
    end
end

function Base.get(dict::RangeDict, key::Int, default::Int)
    for (s, d) in Iterators.zip(dict.srcs, dict.dsts)
        if first(s) <= key <= last(s)
            i = key + 1 - first(s)
            return d[i]
        end
    end

    default
end

parse_file(path) =
    open(path) do f
        seed_line = readline(f)
        @assert startswith(seed_line, "seeds: ")
        seeds = let seed_line = seed_line[7:end]
            seeds = split(seed_line)
            parse.(Int, seeds)
        end

        @assert readline(f) == ""

        maps = Dict{String,RangeDict}()
        while !eof(f)
            name = first(split(readline(f)))
            srcs = UnitRange{Int}[]
            dsts = UnitRange{Int}[]

            while (line = readline(f)) != ""
                dst, src, len = parse.(Int, split(line))
                push!(srcs, src:src+len-1)
                push!(dsts, dst:dst+len-1)
            end

            maps[name] = RangeDict(srcs, dsts)
        end

        seeds, maps
    end

function find_location(maps, code)
    types = [
        "seed",
        "soil",
        "fertilizer",
        "water",
        "light",
        "temperature",
        "humidity",
        "location",
    ]

    for i in Iterators.drop(eachindex(types), 1)
        code = get(maps["$(types[i-1])-to-$(types[i])"], code, code)
    end

    code
end

function part01(path)
    seeds, maps = parse_file(path)
    locations = find_location.(Ref(maps), seeds)
    minimum(locations)
end

function part02(path)
    seed_range_list, maps = parse_file(path)

    seeds = map(Iterators.partition(seed_range_list, 2)) do x
        s, l = x
        s:s+l-1
    end

    types = [
        "seed",
        "soil",
        "fertilizer",
        "water",
        "light",
        "temperature",
        "humidity",
        "location",
    ]

    for i in Iterators.drop(eachindex(types), 1)
        name = "$(types[i-1])-to-$(types[i])"
        mapping = maps[name]

        seeds =
            map(seeds) do seed
                new_seeds = UnitRange{Int}[]

                for j in eachindex(mapping.srcs)
                    src = mapping.srcs[j]
                    dst = mapping.dsts[j]

                    prev_last = if j - 1 < firstindex(mapping.srcs)
                        0
                    else
                        last(mapping.srcs[j-1])
                    end

                    if prev_last < first(seed) < first(src)
                        overlap = first(seed):min(last(seed), first(src) - 1)
                        seed = first(src):last(seed)
                        if length(overlap) > 0
                            push!(new_seeds, overlap)
                        end
                    end

                    if first(src) <= first(seed) <= last(src)
                        overlap = first(seed):min(last(seed), last(src))
                        seed = last(src)+1:last(seed)

                        first_index = first(overlap) + 1 - first(src)
                        last_index = last(overlap) + 1 - first(src)


                        if first_index <= last_index
                            first_dst = dst[first_index]
                            last_dst = dst[last_index]

                            push!(new_seeds, first_dst:last_dst)
                        end
                    end
                end

                src = last(mapping.srcs)

                if last(src) < first(seed) && length(seed) > 0
                    push!(new_seeds, seed)
                end

                new_seeds
            end |>
            Iterators.flatten |>
            collect
    end

    minimum(minimum(seeds))
end

end
