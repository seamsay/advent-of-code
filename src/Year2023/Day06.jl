module Day06

const v₀ = 0
const a = 1

x(total, hold) =
    let v = v₀ + hold * a
        v * (total - hold)
    end

x(total) = x.(total, 0:total)

function part01(path)
    lines = readlines(path)

    @assert startswith(lines[1], "Time:")
    times = parse.(Int, split(lines[1][6:end]))

    @assert startswith(lines[2], "Distance:")
    records = parse.(Int, split(lines[2][10:end]))

    @assert length(times) == length(records)

    map(zip(times, records)) do e
        t, r = e

        length(filter(>(r), x(t)))
    end |> prod
end

function part02(path)
    lines = readlines(path)

    @assert startswith(lines[1], "Time:")
    time = parse(Int, filter(isdigit, lines[1][6:end]))

    @assert startswith(lines[2], "Distance:")
    record = parse.(Int, filter(isdigit, lines[2][10:end]))

    # If tₕ is the time that the button is held and tᵣ is the total race time, the distance traveled x is:
    #     x = tₕ (tᵣ - tₕ) = tᵣ tₕ - tₕ²
    # This is a negative quadratic in tₕ, which is centred on tᵣ/2.
    # This means that if we find the first hold time that will win the race tₛ, then the total number of winning hold times is (bearing in mind that tᵣ/2 may or may not be counted depending on whether it is even):
    #     n = 2 (⌊tᵣ/2⌋ - tₛ) + 1 - (tᵣ % 2)
    # Furthermore, with the record distance denoted by r, tₛ can be calculated as
    #     x = r
    #     r = tₛ (tᵣ - tₛ)
    #     tₛ² - tₛtᵣ + r = 0
    #     (tₛ - tᵣ/2)² - tᵣ²/4 + r = 0
    #     (tₛ - tᵣ/2)² = tᵣ²/4 - r
    #     tₛ - tᵣ/2 = √(tᵣ²/4 - r)
    #     tₛ = tᵣ/2 ± √(tᵣ²/4 - r)
    # and since the first successful hold time is the smaller of these values:
    #     tₛ = tᵣ/2 - √(tᵣ²/4 - r)
    # Though remembering that the button must be held for an integer number of milliseconds, the actual value is the `ceil` of this.

    let tᵣ = time, r = record
        tₛ = ceil(tᵣ / 2 - sqrt(tᵣ^2 / 4 - r))
        n = tᵣ + 1 - 2tₛ
        Int(n)
    end
end

end
