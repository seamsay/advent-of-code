module Day13

function load(path)
    open(path) do f
        matrices = Matrix{Bool}[]

        while !eof(f)
            lines = String[]
            while (line = readline(f)) != ""
                push!(lines, line)
            end

            @assert maximum(length.(lines)) == minimum(length.(lines))
            rs = length(lines)
            cs = length(first(lines))

            m = Matrix{Bool}(undef, rs, cs)
            for (r, line) in enumerate(lines)
                for (c, character) in enumerate(line)
                    m[r, c] = if character == '.'
                        false
                    elseif character == '#'
                        true
                    else
                        error("$character not recognised!")
                    end
                end
            end

            push!(matrices, m)
        end

        matrices
    end
end

function part01(path)
    matrices = load(path)
    map(matrices) do matrix
        for i = 1:size(matrix, 2)-1
            matches = true
            for l = 1:i
                r = 2 * i + 1 - l
                if checkbounds(Bool, matrix, 1, r) && any(matrix[:, l] .!= matrix[:, r])
                    matches = false
                    break
                end
            end

            if matches
                return i
            end
        end

        for i = 1:size(matrix, 1)-1
            matches = true
            for l = 1:i
                r = 2 * i + 1 - l
                if checkbounds(Bool, matrix, r, 1) && any(matrix[l, :] .!= matrix[r, :])
                    matches = false
                    break
                end
            end

            if matches
                return 100 * i
            end
        end
    end |> sum
end

function part02(path)
    matrices = load(path)
    map(matrices) do matrix
        for i = 1:size(matrix, 2)-1
            differ = 0
            for l = 1:i
                r = 2 * i + 1 - l
                if checkbounds(Bool, matrix, 1, r)
                    differ += count(matrix[:, l] .!= matrix[:, r])
                    if differ > 1
                        break
                    end
                end
            end

            if differ == 1
                return i
            end
        end

        for i = 1:size(matrix, 1)-1
            differ = 0
            for l = 1:i
                r = 2 * i + 1 - l
                if checkbounds(Bool, matrix, r, 1)
                    differ += count(matrix[l, :] .!= matrix[r, :])
                    if differ > 1
                        break
                    end
                end
            end

            if differ == 1
                return 100 * i
            end
        end
    end |> sum
end

end
