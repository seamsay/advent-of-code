module Day10

using Graphs

mutable struct Graph
    chars::Matrix{Char}
    graph::SimpleDiGraph{Int}
    labels::Matrix{Int}
    coordinates::Dict{Int,Tuple{Int,Int}}
    start::Tuple{Int,Int}
end

Graph(grid::Matrix{Char}) = Graph(
    grid,
    SimpleDiGraph{Int}(),
    fill(-1, size(grid)),
    Dict{Int,Tuple{Int,Int}}(),
    (-1, -1),
)

function construct(grid)
    graph = Graph(grid)

    for c in axes(grid, 2)
        for r in axes(grid, 1)
            if grid[r, c] != '.'
                add_vertex!(graph.graph)
                label = nv(graph.graph)
                graph.labels[r, c] = label
                graph.coordinates[label] = (r, c)
            end
        end
    end

    for c in axes(grid, 2)
        for r in axes(grid, 1)
            this = graph.labels[r, c]
            if grid[r, c] == 'S'
                graph.start = (r, c)
            elseif grid[r, c] == '|'
                if checkbounds(Bool, graph.labels, r - 1, c)
                    north = graph.labels[r-1, c]
                    add_edge!(graph.graph, this, north)
                end

                if checkbounds(Bool, graph.labels, r + 1, c)
                    south = graph.labels[r+1, c]
                    add_edge!(graph.graph, this, south)
                end
            elseif grid[r, c] == '-'
                if checkbounds(Bool, graph.labels, r, c - 1)
                    west = graph.labels[r, c-1]
                    add_edge!(graph.graph, this, west)
                end

                if checkbounds(Bool, graph.labels, r, c + 1)
                    east = graph.labels[r, c+1]
                    add_edge!(graph.graph, this, east)
                end
            elseif grid[r, c] == 'L'
                if checkbounds(Bool, graph.labels, r - 1, c)
                    north = graph.labels[r-1, c]
                    add_edge!(graph.graph, this, north)
                end

                if checkbounds(Bool, graph.labels, r, c + 1)
                    east = graph.labels[r, c+1]
                    add_edge!(graph.graph, this, east)
                end
            elseif grid[r, c] == 'J'
                if checkbounds(Bool, graph.labels, r - 1, c)
                    north = graph.labels[r-1, c]
                    add_edge!(graph.graph, this, north)
                end

                if checkbounds(Bool, graph.labels, r, c - 1)
                    west = graph.labels[r, c-1]
                    add_edge!(graph.graph, this, west)
                end
            elseif grid[r, c] == '7'
                if checkbounds(Bool, graph.labels, r + 1, c)
                    south = graph.labels[r+1, c]
                    add_edge!(graph.graph, this, south)
                end

                if checkbounds(Bool, graph.labels, r, c - 1)
                    west = graph.labels[r, c-1]
                    add_edge!(graph.graph, this, west)
                end
            elseif grid[r, c] == 'F'
                if checkbounds(Bool, graph.labels, r + 1, c)
                    south = graph.labels[r+1, c]
                    add_edge!(graph.graph, this, south)
                end

                if checkbounds(Bool, graph.labels, r, c + 1)
                    east = graph.labels[r, c+1]
                    add_edge!(graph.graph, this, east)
                end
            end
        end
    end

    r0, c0 = graph.start
    start = graph.labels[r0, c0]
    for dc = -1:1
        for dr = -1:1
            r = r0 + dr
            c = c0 + dc

            if checkbounds(Bool, graph.labels, r, c) && graph.labels[r, c] != -1
                neighbour = graph.labels[r, c]
                if has_edge(graph.graph, neighbour, start)
                    add_edge!(graph.graph, start, neighbour)
                end
            end
        end
    end

    graph
end

function traverse(graph)
    start = graph.labels[graph.start...]
    path = Int[start]
    node = first(outneighbors(graph.graph, start))
    while node != start
        old = node

        ons = outneighbors(graph.graph, node)
        @assert length(ons) == 2
        n1, n2 = ons

        node = if n1 == last(path)
            n2
        elseif n2 == last(path)
            n1
        else
            error("Neither n1 $n1 or n2 $n2 are in the path $path.")
        end

        push!(path, old)
    end

    path
end

function part01(path)
    lines = readlines(path)
    chars = collect.(lines)
    grid = permutedims(stack(chars))
    graph = construct(grid)
    path = traverse(graph)

    div(length(path), 2)
end

function part02(path)
    lines = readlines(path)
    chars = collect.(lines)
    grid = permutedims(stack(chars))
    graph = construct(grid)
    path = traverse(graph)

    pathgrid = fill(false, 3 .* size(grid))
    for label in path
        r, c = graph.coordinates[label]
        ar, ac = (r, c) .* 3 .- 1

        ns = map(l -> graph.coordinates[l], outneighbors(graph.graph, label))
        @assert length(ns) == 2

        pathgrid[ar, ac] = true
        for (rn, cn) in ns
            dr = r - rn
            dc = c - cn
            pathgrid[ar-dr, ac-dc] = true
        end
    end

    queue = Tuple{Int,Int}[]
    seen = Set{Tuple{Int,Int}}()

    R, C = size(pathgrid)
    for r = 1:R
        push!(queue, (r, 1))
        push!(queue, (r, C))
    end
    for c = 1:C
        push!(queue, (1, c))
        push!(queue, (C, c))
    end

    while !isempty(queue)
        coord = popfirst!(queue)
        r, c = coord

        coord in seen && continue
        checkbounds(Bool, pathgrid, r, c) || continue

        push!(seen, coord)
        pathgrid[r, c] && continue

        push!(queue, (r - 1, c))
        push!(queue, (r, c + 1))
        push!(queue, (r + 1, c))
        push!(queue, (r, c - 1))
    end

    i = 0
    for r in axes(grid, 1)
        for c in axes(grid, 2)
            wasseen = false
            for δr = -2:0
                for δc = -2:0
                    if (3r + δr, 3c + δc) in seen
                        wasseen = true
                    end
                end
            end
            if !wasseen
                i += 1
            end
        end
    end

    i
end

end
