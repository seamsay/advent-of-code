module Day14

@enum Rock ABSENT ROLLING STATIONARY

function myprint(m::Matrix{Rock})
    for row in eachrow(m)
        for rock in row
            if rock == ABSENT
                print('.')
            elseif rock == ROLLING
                print('O')
            elseif rock == STATIONARY
                print('#')
            else
                error("??!?!?")
            end
        end
        println()
    end
end

function load(path)
    lines = readlines(path)

    @assert maximum(length.(lines)) == minimum(length.(lines))
    rows = length(lines)
    columns = maximum(length.(lines))

    m = Matrix{Rock}(undef, rows, columns)
    for i = 1:rows
        for j = 1:columns
            code = lines[i][j]
            m[i, j] = if code == '.'
                ABSENT
            elseif code == 'O'
                ROLLING
            elseif code == '#'
                STATIONARY
            else
                error("$code not recognised!")
            end
        end
    end

    m
end

function weight(grid)
    total = 0
    for I in CartesianIndices(grid)
        if grid[I] == ROLLING
            r, _ = Tuple(I)
            total += size(grid, 1) + 1 - r
        end
    end
    total
end

function tilt!(grid)
    for column in axes(grid, 2)
        row = 1
        while row <= size(grid, 2)
            if row == 1 || grid[row-1, column] == STATIONARY
                i = row
                count = 0
                while i <= size(grid, 2) && grid[i, column] != STATIONARY
                    if grid[i, column] == ROLLING
                        count += 1
                        grid[i, column] = ABSENT
                    end

                    i += 1
                end

                for j = row:row+count-1
                    grid[j, column] = ROLLING
                end

                row = i
            end

            row += 1
        end
    end
end

function cycle(grid)
    grid = copy(grid)

    # North
    tilt!(grid)

    # West
    grid = rotr90(grid)
    tilt!(grid)

    # South
    grid = rotr90(grid)
    tilt!(grid)

    # East
    grid = rotr90(grid)
    tilt!(grid)

    rotr90(grid)
end

function part01(path)
    grid = load(path)

    tilt!(grid)
    weight(grid)
end

function part02(path, cycles = 1000000000)
    grid = load(path)

    states = Dict{Matrix{Rock},Int}()

    loop = (-1, -1)
    for i = 1:cycles
        if haskey(states, grid)
            loop = (states[grid], i)
            break
        else
            states[grid] = i
            grid = cycle(grid)
        end
    end

    positions = Dict(v => k for (k, v) in states)

    loop_length = loop[2] - loop[1]
    looped_states = Vector{Matrix{Rock}}(undef, loop_length)
    for i = 1:loop_length
        j = i + loop[1] - 1
        looped_states[i] = positions[j]
    end

    looped_cycles = cycles + 1 - loop[1]
    final_state = (looped_cycles + loop_length - 1) % loop_length + 1
    weight(looped_states[final_state+1])
end

end
