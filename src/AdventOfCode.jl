module AdventOfCode

export Year2021, Year2022, Year2023, Year2024

include("Year2021/Year2021.jl")
include("Year2022/Year2022.jl")
include("Year2023/Year2023.jl")
include("Year2024/Year2024.jl")

end  # module
