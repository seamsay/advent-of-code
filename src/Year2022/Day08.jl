module Day08

using Underscores: @_

readtrees(path) = @_ readlines(path) |>
   map(parse.(Int, collect(_)), __) |>
   map(adjoint, __) |>
   vcat(__...)

function part01(path)
    trees = readtrees(path)

    visible = trues(size(trees))
    visible[2:end-1, 2:end-1] .= false

    # I _think_ this is about as cursed as I can make it...

    # Left-view.
    visible[:, 2:end] .|= accumulate(max, trees; dims = 2)[:, 1:end-1] .< trees[:, 2:end]
    # Top-view.
    visible[2:end, :] .|= accumulate(max, trees; dims = 1)[1:end-1, :] .< trees[2:end, :]
    # Right-view.
    visible[:, 1:end-1] .|= reverse(
        accumulate(max, reverse(trees; dims = 2); dims = 2)[:, 1:end-1] .<
        reverse(trees; dims = 2)[:, 2:end];
        dims = 2,
    )
    # Bottom-view.
    visible[1:end-1, :] .|= reverse(
        accumulate(max, reverse(trees; dims = 1); dims = 1)[1:end-1, :] .<
        reverse(trees; dims = 1)[2:end, :];
        dims = 1,
    )

    sum(visible)
end

function part02(path)
    trees = readtrees(path)
    distance = zero(trees)

    for I in CartesianIndices(trees)
        row, column = Tuple(I)

        left = 0
        for i = 1:column-1
            left += 1
            trees[row, column] <= trees[row, column-i] && break
        end

        right = 0
        for i = 1:size(trees, 2)-column
            right += 1
            trees[row, column] <= trees[row, column+i] && break
        end

        up = 0
        for i = 1:row-1
            up += 1
            trees[row, column] <= trees[row-i, column] && break
        end

        down = 0
        for i = 1:size(trees, 1)-row
            down += 1
            trees[row, column] <= trees[row+i, column] && break
        end

        distance[row, column] = left * right * up * down
    end

    maximum(distance)
end

end
