module Day03

using Underscores: @_

priority(letter) =
    if isuppercase(letter)
        27 + (letter - 'A')
    else
        1 + (letter - 'a')
    end

part01(path) = @_ readlines(path) |>
   map(let m = div(length(_), 2)
       (_[begin:m], _[m+1:end])
   end, __) |>
   map(intersect(_[1], _[2]), __) |>
   map(priority.(_), __) |>
   map(sum, __) |>
   sum

part02(path) = @_ readlines(path) |>
   Iterators.partition(__, 3) |>
   map(intersect(_...), __) |>
   map(only, __) |>
   map(priority, __) |>
   sum

end
