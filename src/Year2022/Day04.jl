module Day04

using Underscores: @_

part01(path) = @_ readlines(path) |>
   map(match(r"^(\d+)-(\d+),(\d+)-(\d+)$", _), __) |>
   map(parse.(Int, _.captures), __) |>
   map((Set(_[1]:_[2]), Set(_[3]:_[4])), __) |>
   map(let l = _[1], r = _[2], u = union(l, r)
       u == l || u == r
   end, __) |>
   sum

part02(path) = @_ readlines(path) |>
   map(match(r"^(\d+)-(\d+),(\d+)-(\d+)$", _), __) |>
   map(parse.(Int, _.captures), __) |>
   map((Set(_[1]:_[2]), Set(_[3]:_[4])), __) |>
   map(!isempty(intersect(_[1], _[2])), __) |>
   sum

end
