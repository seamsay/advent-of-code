module Day07

mutable struct FileSystem
    size::Int
    files::Dict{String,Int}
    directories::Dict{String,FileSystem}
end

FileSystem() = FileSystem(0, Dict(), Dict())

function Base.getindex(fs::FileSystem, path::Vector{String})
    if length(path) == 0
        return fs
    end

    this = fs
    for element in path[begin:end-1]
        this = this.directories[element]
    end

    final = path[end]
    if haskey(this.files, final) && haskey(this.directories, final)
        error("File and directory at path: $path")
    elseif haskey(this.files, final)
        this.files[final]
    elseif haskey(this.directories, final)
        this.directories[final]
    else
        error("No file or directory at path: $path")
    end
end

function Base.setindex!(
    fs::FileSystem,
    item::Union{Int,FileSystem},
    parents::Vector{String},
    name::AbstractString,
)
    directory = fs[parents]

    @assert directory isa FileSystem

    if item isa Int
        directory.files[name] = item
    elseif item isa FileSystem
        directory.directories[name] = item
    else
        error("Type of `item` should have prevented this.")
    end
end

function set_up_filesystem(path)
    io = open(path)
    @assert readuntil(io, raw"$ ") == ""

    path = String[]
    fs = FileSystem()

    while !eof(io)
        line = readuntil(io, '\n')
        captures = match(r"^([^ ]+)(?: (.*))?$", line)

        command = captures[1]
        argument = captures[2]
        output = readuntil(io, raw"$ ")

        @debug "Command read." command argument output

        if command == "cd"
            @assert output == ""
            if argument == "/"
                empty!(path)
            elseif argument == ".."
                pop!(path)
            else
                push!(path, argument)
                @assert fs[path] isa FileSystem
            end
        elseif command == "ls"
            @assert isnothing(argument)
            for line in eachline(IOBuffer(output))
                if (capture = match(r"^dir (.+)$", line)) !== nothing
                    fs[path, capture[1]] = FileSystem()
                elseif (capture = match(r"^(\d+) (.+)$", line)) !== nothing
                    fs[path, capture[2]] = parse(Int, capture[1])
                else
                    error("Invalid output line: $line")
                end
            end
        end
    end

    function set_sizes!(fs)
        total = 0

        for directory in values(fs.directories)
            set_sizes!(directory)
            total += directory.size
        end

        for file in values(fs.files)
            total += file
        end

        fs.size = total
    end
    set_sizes!(fs)

    fs
end

function part01(path)
    fs = set_up_filesystem(path)

    stack = [fs]
    total = 0
    while !isempty(stack)
        node = pop!(stack)
        if node.size <= 100_000
            total += node.size
        end
        append!(stack, values(node.directories))
    end

    total
end

function part02(path)
    fs = set_up_filesystem(path)

    stack = [fs]
    sizes = Int[]
    while !isempty(stack)
        node = pop!(stack)
        push!(sizes, node.size)
        append!(stack, values(node.directories))
    end

    total = 70_000_000
    needed = 30_000_000
    available = total - fs.size
    to_free = needed - available

    filter(>=(to_free), sizes) |> minimum
end

end
