module Day02

using Underscores: @_

@enum Shape rock = 0 paper = 1 scissors = 2

function Shape(letter::AbstractString)
    if letter == "A" || letter == "X"
        rock
    elseif letter == "B" || letter == "Y"
        paper
    elseif letter == "C" || letter == "Z"
        scissors
    else
        error("Unknown letter `$letter`.")
    end
end

function beats(shape::Shape)
    if shape == rock
        paper
    elseif shape == paper
        scissors
    elseif shape == scissors
        rock
    else
        error("Invalid shape `$shape`.")
    end
end

shape_score(shape) = Int(shape) + 1

function win_score(player, opponent)
    if beats(opponent) == player
        6
    elseif player == opponent
        3
    elseif beats(player) == opponent
        0
    else
        error("All outcomes should be covered.")
    end
end

round_score((opponent, player)) = shape_score(player) + win_score(player, opponent)

function part01(io)
    @_ read(io, String) |>
       strip |>
       split(__, '\n') |>
       map(split, __) |>
       map(Shape.(_), __) |>
       map(round_score, __) |>
       sum
end

@enum Result lose = 0 draw = 3 win = 6

function Result(letter::AbstractString)
    if letter == "X"
        lose
    elseif letter == "Y"
        draw
    elseif letter == "Z"
        win
    else
        error("Unrecognised letter `$letter`.")
    end
end


function choice(opponent, result)
    if result == draw
        opponent
    elseif result == win
        beats(opponent)
    elseif result == lose
        only(setdiff([rock, paper, scissors], [opponent, beats(opponent)]))
    else
        error("Invalid result `$result`.")
    end
end

choice_score((opponent, result)) = shape_score(choice(opponent, result)) + Int(result)

function part02(io)
    @_ read(io, String) |>
       strip |>
       split(__, '\n') |>
       map(split, __) |>
       map((Shape(_[1]), Result(_[2])), __) |>
       map(choice_score, __) |>
       sum
end

end
