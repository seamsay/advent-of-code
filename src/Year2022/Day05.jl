module Day05

import DataStructures
using Underscores: @_

function parse_drawing(drawing)
    stack_line, drawing... = reverse(drawing)
    n = @_ split(stack_line) |> parse.(Int, __) |> maximum

    stacks = [Char[] for _ = 1:n]
    for line in drawing
        for i = 1:n
            character = line[(i-1)*4+2]
            if character != ' '
                push!(stacks[i], character)
            end
        end
    end

    stacks
end

function execute1!(stacks, n, from, to)
    for _ = 1:n
        push!(stacks[to], pop!(stacks[from]))
    end
end

function execute2!(stacks, n, from, to)
    n_to_move = @_ length.(stacks) |> maximum
    to_move = DataStructures.CircularDeque{Char}(n_to_move)
    for _ = 1:n
        pushfirst!(to_move, pop!(stacks[from]))
    end
    append!(stacks[to], to_move)
end

function solution(path, executor!)
    lines = readlines(path)
    empty = only(findall(==(""), lines))

    drawing = lines[1:empty-1]
    moves = lines[empty+1:end]

    stacks = parse_drawing(drawing)

    for move in moves
        captures = match(r"^move (\d+) from (\d+) to (\d+)$", move)
        n, from, to = parse.(Int, captures.captures)
        executor!(stacks, n, from, to)
    end

    string(last.(stacks)...)
end

part01(path) = solution(path, execute1!)
part02(path) = solution(path, execute2!)

end
