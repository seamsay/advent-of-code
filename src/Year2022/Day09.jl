module Day09

function execute(path, n)
    knots = fill(0im, n)
    visited = [Set([0im]) for _ = 1:n]

    for line in eachline(path)
        direction, count_str = split(line)
        delta = if direction == "R"
            1 + 0im
        elseif direction == "L"
            -1 + 0im
        elseif direction == "U"
            1im
        elseif direction == "D"
            -1im
        else
            error("Could not recognise direction: $direction")
        end
        count = parse(Int, count_str)

        for _ = 1:count
            knots[1] += delta
            for i = 2:n
                difference = knots[i-1] - knots[i]

                if abs(difference) > sqrt(2)
                    theta = angle(difference)
                    if isapprox(theta, 0)
                        knots[i] += 1
                    elseif isapprox(theta, pi)
                        knots[i] -= 1
                    elseif isapprox(theta, pi / 2)
                        knots[i] += im
                    elseif isapprox(theta, -pi / 2)
                        knots[i] -= im
                    else
                        quadrant = floor(Int, theta / (pi / 2))
                        if quadrant == 0
                            knots[i] += 1 + 1im
                        elseif quadrant == 1
                            knots[i] += -1 + 1im
                        elseif quadrant == -2
                            knots[i] += -1 - 1im
                        elseif quadrant == -1
                            knots[i] += 1 - 1im
                        end
                    end
                    push!(visited[i], knots[i])
                end
            end
        end
    end

    length(visited[end])
end

part01(path) = execute(path, 2)
part02(path) = execute(path, 10)

end
