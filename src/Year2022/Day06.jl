module Day06

import DataStructures

import DataStructures

function find_marker(path, distinct)
    buffer = DataStructures.CircularBuffer{UInt8}(distinct)

    bytes = read(path)

    for byte in bytes[1:distinct-1]
        push!(buffer, byte)
    end

    for (i, byte) in enumerate(bytes[distinct:end])
        push!(buffer, byte)
        if length(Set(buffer)) == length(buffer)
            return i + distinct - 1
        end
    end
end

part01(path) = find_marker(path, 4)
part02(path) = find_marker(path, 14)

end
