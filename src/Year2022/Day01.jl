module Day01

using Underscores: @_

function calories(io)
    @_ read(io, String) |>
       strip |>
       split(__, "\n\n") |>
       split.(__, '\n') |>
       map(parse.(Int, _), __) |>
       map(sum, __)
end

function part01(io)
    calories(io) |> maximum
end

function part02(io)
    @_ calories(io) |> sort(__, rev = true) |> Iterators.take(__, 3) |> sum
end

end
