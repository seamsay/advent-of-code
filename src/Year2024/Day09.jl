module Day09

function part01(path)
    diskmap = parse.(Int, collect(readchomp(path)))
    @assert isodd(length(diskmap))

    start_idx = firstindex(diskmap)
    start_id = 0

    move_idx = lastindex(diskmap)
    move_id = div(length(diskmap) - 1, 2)
    move_count = diskmap[move_idx]

    position = 0
    total = 0
    while start_id < move_id
        for _ = 1:diskmap[start_idx]
            total += position * start_id
            position += 1
        end

        start_id += 1
        start_idx += 1

        if !(start_id < move_id)
            break
        end

        for _ = 1:diskmap[start_idx]
            if move_count == 0
                move_idx -= 2
                move_id -= 1
                move_count = diskmap[move_idx]
            end

            total += position * move_id
            position += 1
            move_count -= 1
        end

        start_idx += 1
    end

    @assert start_id == move_id

    if move_count == 0
        for _ = 1:diskmap[start_idx]
            total += position * start_id
            position += 1
        end
    end

    for _ = 1:move_count
        total += position * move_id
        position += 1
    end

    total
end

function part02(path)
    diskmap = parse.(Int, collect(readchomp(path)))
    @assert isodd(length(diskmap))

    # TODO: Use better data structures (DataStructures.MutableLinkedList doesn't work).
    sizes = diskmap[begin:2:end]
    files = collect(1:length(sizes)) .- 1
    frees = diskmap[begin+1:2:end-1]
    push!(frees, 0)

    moved = Set{Int}()

    file_idx = lastindex(files)
    while checkbounds(Bool, files, file_idx)
        s = sizes[file_idx]
        i = files[file_idx]
        free_idx = findfirst(>=(s), frees)

        if !in(i, moved) && free_idx !== nothing && free_idx < file_idx
            frees[file_idx-1] += frees[file_idx] + s
            frees[free_idx] -= s

            deleteat!(frees, file_idx)
            insert!(frees, free_idx, 0)

            deleteat!(files, file_idx)
            insert!(files, free_idx + 1, i)

            deleteat!(sizes, file_idx)
            insert!(sizes, free_idx + 1, s)

            push!(moved, i)
        else
            file_idx -= 1
        end

        @assert length(files) == length(sizes)
        @assert length(frees) == length(sizes)
    end

    total = 0
    position = 0
    for (i, s, f) in zip(files, sizes, frees)
        for _ = 1:s
            total += position * i
            position += 1
        end

        position += f
    end
    total
end

end  # module
