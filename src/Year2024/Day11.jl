module Day11

function increase!(dict, key, count)
    if !haskey(dict, key)
        dict[key] = 0
    end

    dict[key] += count
end

function run(path, times)
    start = parse.(Int, split(read(path, String)))

    current = Dict{Int,Int}(s => 0 for s in start)
    for stone in start
        current[stone] += 1
    end
    next = empty(current)

    for _ = 1:times
        empty!(next)

        for (value, count) in current
            digits = floor(log10(value)) + 1
            if value == 0
                increase!(next, value + 1, count)
            elseif iseven(digits)
                power = Int(digits) ÷ 2
                left, right = divrem(value, 10^power)
                increase!(next, left, count)
                increase!(next, right, count)
            else
                increase!(next, 2024 * value, count)
            end
        end

        current, next = next, current
    end

    sum(values(current))
end

part01(path) = run(path, 25)

part02(path) = run(path, 75)

end  # module
