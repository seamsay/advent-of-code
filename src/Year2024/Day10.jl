module Day10

using Graphs: Graphs

struct Graph
    graph::Graphs.SimpleDiGraph{Int}
    nodes::Dict{CartesianIndex,Int}
    coords::Dict{Int,CartesianIndex}
end

Graph() = Graph(Graphs.SimpleDiGraph(), Dict(), Dict())

function Graph(grid::Matrix{Int})
    graph = Graph()

    for I in CartesianIndices(grid)
        Graphs.add_vertex!(graph.graph)
        graph.nodes[I] = Graphs.nv(graph.graph)
        graph.coords[Graphs.nv(graph.graph)] = I
    end

    for I in CartesianIndices(grid)
        for J in
            I .+ [
            CartesianIndex(0, 1),
            CartesianIndex(1, 0),
            CartesianIndex(0, -1),
            CartesianIndex(-1, 0),
        ]
            if checkbounds(Bool, grid, J) && grid[J] - grid[I] == 1
                Graphs.add_edge!(graph.graph, graph.nodes[I], graph.nodes[J])
            end
        end
    end

    graph
end

read(path) = parse.(Int, permutedims(hcat(collect.(readlines(path))...)))

function part01(path)
    grid = read(path)
    graph = Graph(grid)

    heads = findall(==(0), grid)
    scores = Dict{CartesianIndex,Int}(h => 0 for h in heads)
    for h in heads
        reachable = Set{CartesianIndex}()
        stack = [h]

        while !isempty(stack)
            coord = pop!(stack)
            for neighbour in Graphs.outneighbors(graph.graph, graph.nodes[coord])
                I = graph.coords[neighbour]
                if grid[I] == 9
                    push!(reachable, I)
                else
                    push!(stack, I)
                end
            end
        end

        scores[h] = length(reachable)
    end

    sum(values(scores))
end

function part02(path)
    grid = read(path)
    graph = Graph(grid)

    heads = findall(==(0), grid)
    ratings = Dict{CartesianIndex,Int}(h => 0 for h in heads)
    for h in heads
        distinct = Set{Vector{CartesianIndex}}()
        stack = [[h]]

        while !isempty(stack)
            path = pop!(stack)
            coord = path[end]

            for neighbour in Graphs.neighbors(graph.graph, graph.nodes[coord])
                I = graph.coords[neighbour]
                path = deepcopy(path)
                push!(path, I)

                if grid[I] == 9
                    push!(distinct, path)
                else
                    push!(stack, path)
                end
            end
        end

        ratings[h] = length(distinct)
    end

    sum(values(ratings))
end

end  # module
