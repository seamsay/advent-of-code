module Day01

function part01(path)
    left, right = open(path) do io
        rows = []
        while !eof(io)
            line = readline(io)
            row = parse.(Int, split(line))
            push!(rows, row)
        end
        eachrow(hcat(rows...))
    end

    sort!(left)
    sort!(right)

    sum(abs.(left .- right))
end

function part02(path)
    ids, counts = open(path) do io
        ids = []
        counts = Dict{Int,Int}()

        while !eof(io)
            line = readline(io)
            id, other = parse.(Int, split(line))

            if !haskey(counts, other)
                counts[other] = 0
            end

            push!(ids, id)
            counts[other] += 1
        end

        ids, counts
    end

    map(id -> id * get(counts, id, 0), ids) |> sum
end

end  # module
