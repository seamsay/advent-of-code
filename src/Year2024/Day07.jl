module Day07

function run(path, concat)
    equations = open(path) do io
        equations = []

        while !eof(io)
            line = readline(io)

            result_str, operands_str = split(line, ':')
            result = parse(Int, result_str)
            operands = parse.(Int, split(operands_str))

            push!(equations, (result, operands))
        end

        equations
    end

    total = 0
    for (result, (first, rest...)) in equations
        stack = [first]
        for r in rest
            new_stack = []
            for l in stack
                push!(new_stack, l + r)
                push!(new_stack, l * r)

                if concat
                    push!(new_stack, l * 10^ceil(Int, log10(r + 1)) + r)
                end
            end
            stack = new_stack
        end

        if any(==(result), stack)
            total += result
        end
    end

    total
end

part01(path) = run(path, false)
part02(path) = run(path, true)

end  # module
