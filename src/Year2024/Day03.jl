module Day03

function part01(path)
    program = read(path, String)
    matches = eachmatch(r"mul\((\d{1,3}),(\d{1,3})\)", program)
    numbers = map(m -> parse.(Int, m), matches)
    products = map(prod, numbers)
    sum(products)
end

function part02(path)
    program = read(path, String)

    enable = true
    total = 0
    for m in eachmatch(r"mul\((\d{1,3}),(\d{1,3})\)|don't\(\)|do\(\)", program)
        if m.match == "don't()"
            enable = false
        elseif m.match == "do()"
            enable = true
        elseif enable
            total += prod(parse.(Int, m.captures))
        end
    end

    total
end

end  # module
