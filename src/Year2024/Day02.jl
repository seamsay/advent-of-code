module Day02

function part01(path)
    levels = open(path) do io
        levels = []
        while !eof(io)
            push!(levels, parse.(Int, split(readline(io))))
        end
        levels
    end

    map(levels) do level
        diffs = diff(level)
        all(1 .<= diffs .<= 3) || all(-3 .<= diffs .<= -1)
    end |> sum
end

function part02(path)
    levels = open(path) do io
        levels = []
        while !eof(io)
            push!(levels, parse.(Int, split(readline(io))))
        end
        levels
    end

    map(levels) do level
        for i in eachindex(level)
            this = deleteat!(copy(level), i)
            diffs = diff(this)
            if all(1 .<= diffs .<= 3) || all(-3 .<= diffs .<= -1)
                return true
            end
        end
        false
    end |> sum
end

end  # module
