module Day16

using DataStructures: DataStructures
using Graphs: Graphs
using SimpleWeightedGraphs: SimpleWeightedGraphs

function dijkstra(graph, start_coord, finish_position)
    dist = [typemax(Int) for _ in Graphs.vertices(graph.graph)]
    dist[graph.nodes[start_coord]] = 0

    @assert Set(n for (n, _) in enumerate(dist)) == Set(Graphs.vertices(graph.graph))

    queue = DataStructures.PriorityQueue{Int,Int}(n => d for (n, d) in enumerate(dist))
    prev = Dict{Int,Int}()

    while !isempty(queue)
        node = DataStructures.dequeue!(queue)
        for neighbour in Graphs.outneighbors(graph.graph, node)
            new_dist =
                dist[node] + SimpleWeightedGraphs.get_weight(graph.graph, node, neighbour)
            if new_dist < dist[neighbour]
                prev[neighbour] = node
                dist[neighbour] = new_dist
                queue[neighbour] = new_dist
            end
        end
    end

    map([
        CartesianIndex(0, 1),
        CartesianIndex(0, -1),
        CartesianIndex(1, 0),
        CartesianIndex(-1, 0),
    ]) do d
        coord = Coord(finish_position, d)
        if !haskey(graph.nodes, coord)
            return typemax(Int)
        end

        node = graph.nodes[coord]
        dist[node]
    end |> minimum
end

struct Coord
    position::CartesianIndex{2}
    direction::CartesianIndex{2}
end

struct Graph
    graph::SimpleWeightedGraphs.SimpleWeightedDiGraph{Int,Int}
    nodes::Dict{Coord,Int}
    coords::Dict{Int,Coord}
end

Graph() = Graph(
    fieldtype(Graph, :graph)(),
    fieldtype(Graph, :nodes)(),
    fieldtype(Graph, :coords)(),
)

rotl90(v::CartesianIndex{2}) =
    let (x, y) = Tuple(v)
        CartesianIndex(-y, x)
    end

rotr90(v::CartesianIndex{2}) =
    let (x, y) = Tuple(v)
        CartesianIndex(y, -x)
    end

strdir(c) =
    if c == CartesianIndex(0, 1)
        '>'
    elseif c == CartesianIndex(1, 0)
        'v'
    elseif c == CartesianIndex(0, -1)
        '<'
    elseif c == CartesianIndex(-1, 0)
        '^'
    else
        error()
    end


function part01(path)
    grid = open(path) do io
        lines = readlines(path)
        char_lines = collect.(lines)
        transposed_grid = hcat(char_lines...)
        permutedims(transposed_grid)
    end

    start = only(findall(==('S'), grid))
    finish = only(findall(==('E'), grid))

    start_coord = Coord(start, CartesianIndex(0, 1))
    graph = Graph()
    Graphs.add_vertex!(graph.graph)
    graph.nodes[start_coord] = Graphs.nv(graph.graph)
    graph.coords[Graphs.nv(graph.graph)] = start_coord

    visited = Set{Coord}()
    stack = [start_coord]
    while !isempty(stack)
        coord = pop!(stack)
        coord in visited && continue
        push!(visited, coord)

        for (position, direction, cost) in (
            (coord.position, rotl90(coord.direction), 1000),
            (coord.position + coord.direction, coord.direction, 1),
            (coord.position, rotr90(coord.direction), 1000),
        )
            let (x, y) = Tuple(direction)
                @assert (abs(x) == 1 && y == 0) || (x == 0 && abs(y) == 1)
            end

            new_coord = Coord(position, direction)
            if grid[new_coord.position] != '#'
                if !haskey(graph.nodes, new_coord)
                    Graphs.add_vertex!(graph.graph)
                    graph.nodes[new_coord] = Graphs.nv(graph.graph)
                    graph.coords[Graphs.nv(graph.graph)] = new_coord
                end

                Graphs.add_edge!(
                    graph.graph,
                    graph.nodes[coord],
                    graph.nodes[new_coord],
                    cost,
                )
                push!(stack, new_coord)
            end
        end
    end

    dijkstra(graph, start_coord, finish)
end

function part02(path) end

end  # module
