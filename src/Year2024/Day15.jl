module Day15

using Graphs: Graphs

function load(path)
    lines = readlines(path)
    separator = only(findall(==(""), lines))

    grid = permutedims(hcat(collect.(lines[begin:begin+separator-2])...))
    start = only(findall(==('@'), grid))

    move_str = Char[]
    for l in lines[begin+separator:end]
        append!(move_str, collect(l))
    end

    moves = map(move_str) do m
        if m == '<'
            CartesianIndex(0, -1)
        elseif m == '^'
            CartesianIndex(-1, 0)
        elseif m == '>'
            CartesianIndex(0, 1)
        elseif m == 'v'
            CartesianIndex(1, 0)
        else
            error()
        end
    end

    start, grid, moves
end

function print_grid(grid)
    println("-----")
    for r in eachrow(grid)
        for c in r
            print(c)
        end
        println()
    end
end

function part01(path)
    p, grid, moves = load(path)

    for v in moves
        f = p + v
        while grid[f] == 'O'
            f += v
        end

        if grid[f] == '#'
            continue
        elseif grid[f] != '.'
            error()
        end

        while f != p
            grid[f] = 'O'
            f -= v
            grid[f] = '.'
        end

        grid[p] = '.'
        p += v
        grid[p] = '@'
    end

    total = 0
    for I in CartesianIndices(grid)
        if grid[I] == 'O'
            x, y = Tuple(I)

            total += 100(x - 1) + y - 1
        end
    end

    total
end

function part02(path)
    old_start, old_grid, moves = load(path)

    osr, osc = Tuple(old_start)
    p = CartesianIndex(osr, 2osc - 1)

    h, w = size(old_grid)
    grid = fill('\0', h, 2w)
    for I in CartesianIndices(old_grid)
        c = old_grid[I]
        ir, ic = Tuple(I)
        J = CartesianIndex(ir, 2ic - 1)

        grid[J] = if c == '#'
            '#'
        elseif c == 'O'
            '['
        elseif c == '.'
            '.'
        elseif c == '@'
            '@'
        else
            error()
        end

        grid[J+CartesianIndex(0, 1)] = if c == '#'
            '#'
        elseif c == 'O'
            ']'
        elseif c == '.'
            '.'
        elseif c == '@'
            '.'
        else
            error()
        end
    end

    for v in moves
        checked = Set{CartesianIndex}()
        to_check = [p + v]
        boxes = Set{NTuple{2,CartesianIndex}}()
        while !isempty(to_check)
            c = pop!(to_check)
            c in checked && continue
            push!(checked, c)

            b = grid[c]

            if b == '#'
                @goto next
            elseif b == '['
                other = CartesianIndex(0, 1)
                push!(boxes, (c, c + other))

                push!(to_check, c + v)
                push!(to_check, c + v + other)
            elseif b == ']'
                other = CartesianIndex(0, -1)
                push!(boxes, (c + other, c))

                push!(to_check, c + v)
                push!(to_check, c + v + other)
            elseif b == '@'
                error()
            end
        end

        g = Graphs.SimpleDiGraph()
        box_nodes = Dict{NTuple{2,CartesianIndex},Int}()
        node_boxes = Dict{Int,NTuple{2,CartesianIndex}}()
        for b in boxes
            Graphs.add_vertex!(g)
            box_nodes[b] = Graphs.nv(g)
            node_boxes[Graphs.nv(g)] = b
        end

        for (n1, b1) in node_boxes, (n2, b2) in node_boxes
            l1, r1 = b1
            l2, r2 = b2

            if l1 == l2 || r1 == r2
                continue
            end

            if l1 + v == r2 || l1 + v == l2 || r1 + v == l2
                Graphs.add_edge!(g, n2, n1)
            elseif l2 + v == r1 || l2 + v == l1 || r2 + v == l1
                Graphs.add_edge!(g, n1, n2)
            end
        end

        for n in Graphs.topological_sort(g)
            l, r = node_boxes[n]
            @assert l + v == r || r + v == l || (grid[r+v] == '.' && grid[l+v] == '.')
            grid[l] = '.'
            grid[r] = '.'
            grid[l+v] = '['
            grid[r+v] = ']'
        end

        grid[p] = '.'
        p += v
        grid[p] = '@'

        @label next
    end

    total = 0
    for I in CartesianIndices(grid)
        if grid[I] != '['
            continue
        end

        r, c = Tuple(I)

        total += 100(r - 1) + c - 1
    end
    total
end

end  # module
