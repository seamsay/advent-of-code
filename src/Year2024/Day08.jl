module Day08

read(path) = permutedims(hcat(collect.(readlines(path))...))

function locations(grid)
    antennae = Dict{Char,Set{CartesianIndex}}()

    for I in CartesianIndices(grid)
        if isletter(grid[I]) || isdigit(grid[I])
            push!(get!(antennae, grid[I], Set{CartesianIndex}()), I)
        end
    end

    antennae
end

function part01(path)
    grid = read(path)
    antennae = locations(grid)

    antinodes = Set{CartesianIndex}()
    for locs in values(antennae)
        for l in locs, r in locs
            l == r && continue

            an_loc = 2l - r
            if checkbounds(Bool, grid, an_loc)
                push!(antinodes, an_loc)
            end
        end
    end

    length(antinodes)
end

function part02(path)
    grid = read(path)
    antennae = locations(grid)

    antinodes = Set{CartesianIndex}()
    for locs in values(antennae)
        for l in locs, r in locs
            l == r && continue

            for (start, direction) in ((l, l - r), (r, r - l))
                for i in Iterators.countfrom(0)
                    loc = start + i * direction
                    checkbounds(Bool, grid, loc) || break
                    push!(antinodes, loc)
                end
            end
        end
    end

    length(antinodes)
end

end  # module
