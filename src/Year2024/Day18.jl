module Day18

using DataStructures: DataStructures

function load(path)
    lines = readlines(path)
    pairs = map(Tuple, split.(lines, ','))
    coords = map(p -> parse.(Int, p), pairs)
    coords
end

function traverse(bytes, edge)
    visited = Set{NTuple{2,Int}}()
    queue = DataStructures.PriorityQueue((0, 0) => 0)

    while !isempty(queue)
        position, steps = DataStructures.dequeue_pair!(queue)
        push!(visited, position)

        for direction in ((0, 1), (0, -1), (1, 0), (-1, 0))
            new_steps = steps + 1
            new_position = position .+ direction

            if new_position == (edge, edge)
                return new_steps
            elseif all((0, 0) .<= new_position .<= (edge, edge)) &&
                   !in(new_position, bytes) &&
                   !in(new_position, visited)
                @assert !haskey(queue, new_position) || queue[new_position] == new_steps
                queue[new_position] = new_steps
            end
        end

    end
end

function part01(path, edge = 70, t = 1024)
    bytes = Set(load(path)[begin:begin+t-1])

    traverse(bytes, edge)
end

function part02(path, edge = 70, t = 1024)
    bytes = load(path)
    fallen = Set(bytes[begin:begin+t-1])
    for falling in bytes[begin+t:end]
        push!(fallen, falling)
        if traverse(fallen, edge) === nothing
            return falling
        end
    end
end

end  # module
