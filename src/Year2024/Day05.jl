module Day05

using Graphs: Graphs

struct Graph
    graph::Graphs.SimpleDiGraph{Int}
    nodes::Dict{Int,Int}
    pages::Dict{Int,Int}
end

Graph() = Graph(Graphs.SimpleDiGraph(), Dict(), Dict())

function read(path)
    open(path) do io
        dependencies = Graph()
        while !eof(io)
            line = readline(io)
            line == "" && break

            pages = parse.(Int, split(line, '|'))

            for page in pages
                if !haskey(dependencies.nodes, page)
                    Graphs.add_vertex!(dependencies.graph)
                    dependencies.nodes[page] = Graphs.nv(dependencies.graph)
                    dependencies.pages[Graphs.nv(dependencies.graph)] = page
                end
            end

            before, after = pages
            Graphs.add_edge!(
                dependencies.graph,
                dependencies.nodes[before],
                dependencies.nodes[after],
            )
        end

        updates = []
        while !eof(io)
            push!(updates, parse.(Int, split(readline(io), ',')))
        end

        dependencies, updates
    end
end

function part01(path)
    dependencies, updates = read(path)

    total = 0
    for update in updates
        valid = true
        for (i, before) in enumerate(update)
            valid =
                !any(update[begin+i:end]) do after
                    haskey(dependencies.nodes, before) &&
                        haskey(dependencies.nodes, after) &&
                        Graphs.has_edge(
                            dependencies.graph,
                            dependencies.nodes[after],
                            dependencies.nodes[before],
                        )
                end
            valid || break
        end

        if valid
            total += update[ceil(Int, length(update) / 2)]
        end
    end

    total
end

function part02(path)
    dependencies, updates = read(path)

    total = 0
    for update in updates
        valid = true
        for (i, before) in enumerate(update)
            valid =
                !any(update[begin+i:end]) do after
                    haskey(dependencies.nodes, before) &&
                        haskey(dependencies.nodes, after) &&
                        Graphs.has_edge(
                            dependencies.graph,
                            dependencies.nodes[after],
                            dependencies.nodes[before],
                        )
                end
            valid || break
        end

        if !valid
            subgraph, subgraph_map = Graphs.induced_subgraph(
                dependencies.graph,
                [dependencies.nodes[page] for page in update],
            )
            order = Graphs.topological_sort(subgraph)
            total += dependencies.pages[subgraph_map[order[ceil(Int, length(order) / 2)]]]
        end
    end

    total
end

end  # module
