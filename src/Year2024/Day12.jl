module Day12

using Graphs: Graphs

struct Graph
    graph::Graphs.SimpleGraph{Int}
    nodes::Dict{CartesianIndex,Int}
    coords::Dict{Int,CartesianIndex}
end

Graph() = Graph(Graphs.SimpleDiGraph(), Dict(), Dict())

function Graph(grid::Matrix)
    graph = Graphs.SimpleGraph{Int}()
    nodes = Dict{CartesianIndex,Int}()
    coords = Dict{Int,CartesianIndex}()

    for I in CartesianIndices(grid)
        Graphs.add_vertex!(graph)
        node = Graphs.nv(graph)
        nodes[I] = node
        coords[node] = I
    end

    visited = Set{CartesianIndex}()
    for start in CartesianIndices(grid)
        start in visited && continue

        stack = [start]
        while !isempty(stack)
            I = pop!(stack)
            push!(visited, I)

            for d in CartesianIndex.(((0, 1), (1, 0), (0, -1), (-1, 0)))
                J = I + d
                if checkbounds(Bool, grid, J) && grid[J] == grid[I] && !in(J, visited)
                    Graphs.add_edge!(graph, nodes[I], nodes[J])
                    push!(stack, J)
                end
            end
        end
    end

    Graph(graph, nodes, coords)
end

load(path) = permutedims(hcat(collect.(readlines(path))...))

function part01(path)
    grid = load(path)
    graph = Graph(grid)

    visited = Set{CartesianIndex}()
    areas = Int[]
    perimeters = Int[]
    for start in CartesianIndices(grid)
        start in visited && continue

        area = 0
        perimeter = 0
        stack = [start]
        while !isempty(stack)
            I = pop!(stack)
            I in visited && continue
            push!(visited, I)

            nodes = Graphs.neighbors(graph.graph, graph.nodes[I])

            area += 1
            perimeter += 4 - length(nodes)

            for n in nodes
                push!(stack, graph.coords[n])
            end
        end

        push!(areas, area)
        push!(perimeters, perimeter)
    end

    sum(areas .* perimeters)
end

function part02(path)
    grid = load(path)
    graph = Graph(grid)

    visited = Set{CartesianIndex}()
    areas = Int[]
    sides = Int[]
    for start in CartesianIndices(grid)
        start in visited && continue

        area = 0
        corners = 0

        stack = [start]
        while !isempty(stack)
            I = pop!(stack)
            I in visited && continue
            push!(visited, I)

            nodes = Graphs.neighbors(graph.graph, graph.nodes[I])

            area += 1
            corners += if length(nodes) == 0
                4
            elseif length(nodes) == 1
                2
            elseif length(nodes) == 2
                j, k = nodes

                It = Tuple(I)
                Jt = Tuple(graph.coords[j])
                Kt = Tuple(graph.coords[k])

                Lt = @. Jt + Kt - It
                L = CartesianIndex(Lt)

                if any(Jt .== Kt)
                    0
                elseif grid[L] == grid[I]
                    1
                else
                    2
                end
            elseif length(nodes) == 3 || length(nodes) == 4
                c = 0

                for j in nodes, k in nodes
                    It = Tuple(I)

                    Jt = Tuple(graph.coords[j])
                    Kt = Tuple(graph.coords[k])

                    Lt = @. Jt + Kt - It
                    L = CartesianIndex(Lt)

                    c += if any(Jt .== Kt) || grid[L] == grid[I]
                        0
                    else
                        1
                    end
                end

                @assert iseven(c)
                c ÷ 2
            else
                error("Can't have $(length(nodes)) neighbours!")
            end

            for n in nodes
                push!(stack, graph.coords[n])
            end
        end

        push!(areas, area)
        push!(sides, corners)
    end

    sum(areas .* sides)
end

end  # module
