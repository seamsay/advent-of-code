module Day06

read(path) = permutedims(hcat(collect.(readlines(path))...))

mkindex(m, I) =
    let rows = size(m, 1)
        CartesianIndex(rows + 1 - imag(I), real(I))
    end

Base.getindex(m::Matrix, I::Complex{Int}) = m[mkindex(m, I)]
Base.setindex!(m::Union{Matrix,BitMatrix}, v, I::Complex{Int}) =
    setindex!(m, v, mkindex(m, I))

Base.checkbounds(::Type{Bool}, m::Matrix, I::Complex{Int}) =
    checkbounds(Bool, m, imag(I), real(I))

function part01(path)
    grid = read(path)

    start_coord = only(findall(==('^'), grid))
    grid[start_coord] = '.'

    visited = falses(size(grid))

    position = let
        start_tuple = Tuple(start_coord)
        rows = size(grid, 1)
        start_tuple[2] + (rows + 1 - start_tuple[1]) * im
    end

    direction = 0 + im
    while checkbounds(Bool, grid, position)
        visited[position] = true

        if checkbounds(Bool, grid, position + direction) && grid[position+direction] == '#'
            direction *= -im
            continue
        end

        position += direction
    end

    sum(visited)
end

function part02(path)
    function loops(grid, position, direction, visited)
        grid = deepcopy(grid)
        visited = deepcopy(visited)

        grid[position+direction] = '#'
        direction *= -im

        while checkbounds(Bool, grid, position)
            if direction in visited[position]
                return true
            end

            push!(visited[position], direction)
            if checkbounds(Bool, grid, position + direction) &&
               grid[position+direction] == '#'
                direction *= -im
                continue
            end

            position += direction
        end

        false
    end

    grid = read(path)

    start_coord = only(findall(==('^'), grid))
    grid[start_coord] = '.'

    visited = [Set{Complex{Int}}() for _ = 1:size(grid, 1), _ = 1:size(grid, 2)]
    direction = 0 + im
    position = let
        start_tuple = Tuple(start_coord)
        rows = size(grid, 1)
        start_tuple[2] + (rows + 1 - start_tuple[1]) * im
    end
    obstacles = Set{Complex{Int}}()
    while checkbounds(Bool, grid, position)
        push!(visited[position], direction)

        if checkbounds(Bool, grid, position + direction) && grid[position+direction] == '#'
            direction *= -im
            continue
        end

        next = position + direction

        if checkbounds(Bool, grid, next) &&  # If the next position is out of bound then can't place an obstacle there.
           isempty(visited[next]) &&  # If the guard has already visited the next position then there can't have been an obstacle there.
           grid[next] != '#' &&  # If the next position is already an obstacle then we can't place an obstacle there.
           !in(next, obstacles) &&  # If we've already tried to place an obstacle there then there's no point trying again.
           loops(grid, position, direction, visited)
            push!(obstacles, position + direction)
        end

        position += direction
    end

    length(obstacles)
end

end  # module
