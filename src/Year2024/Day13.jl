module Day13

function load(path)
    equations = Tuple{Matrix{Int},Vector{Int}}[]
    open(path) do io
        while !eof(io)
            ax, ay = match(r"^Button A: X\+(\d\d), Y\+(\d\d)$", readline(io)).captures
            bx, by = match(r"^Button B: X\+(\d\d), Y\+(\d\d)$", readline(io)).captures
            vx, vy = match(r"^Prize: X=(\d+), Y=(\d+)$", readline(io)).captures

            m = parse.(Int, [ax bx; ay by])
            v = parse.(Int, [vx, vy])

            push!(equations, (m, v))

            readline(io)
        end
    end

    equations
end

function part01(path)
    equations = load(path)

    map(equations) do (m, v)
        det = m[1, 1] * m[2, 2] - m[1, 2] * m[2, 1]
        a = (m[2, 2] * v[1] - m[1, 2] * v[2]) / det
        b = (m[1, 1] * v[2] - m[2, 1] * v[1]) / det

        af, ai = modf(a)
        bf, bi = modf(b)

        if af ≈ 0 && bf ≈ 0
            3Int(ai) + Int(bi)
        else
            0
        end
    end |> sum
end

function part02(path)
    equations = load(path)

    map(equations) do (m, v)
        v .+= 10000000000000
        det = m[1, 1] * m[2, 2] - m[1, 2] * m[2, 1]
        a = (m[2, 2] * v[1] - m[1, 2] * v[2]) / det
        b = (m[1, 1] * v[2] - m[2, 1] * v[1]) / det

        af, ai = modf(a)
        bf, bi = modf(b)

        if af ≈ 0 && bf ≈ 0
            3Int(ai) + Int(bi)
        else
            0
        end
    end |> sum
end

end  # module
