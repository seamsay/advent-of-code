module Day04

function part01(path)
    grid = hcat(collect.(readlines(path))...)

    total = 0
    for I in CartesianIndices(grid)
        found = Dict(CartesianIndex(x, y) => true for x = -1:1 for y = -1:1)

        for (distance, L) in enumerate(('X', 'M', 'A', 'S'))
            for direction in keys(found)
                point = I + (distance - 1) * direction

                found[direction] &= checkbounds(Bool, grid, point) && grid[point] == L
            end
        end

        total += sum(values(found))
    end

    total
end

function Base.CartesianIndex(i::Complex{Int})
    CartesianIndex(real(i), imag(i))
end

function part02(path)
    grid = hcat(collect.(readlines(path))...)

    total = 0
    for I in CartesianIndices(grid)
        for orientation in (1 + 0im, 0 + im, -1 + 0im, 0 - im)
            found = true

            for (j, L) in enumerate(('M', 'A', 'S'))
                distance = 2 - j
                middle = I + distance * CartesianIndex(orientation)
                neg = middle + CartesianIndex(distance * orientation * -im)
                pos = middle + CartesianIndex(distance * orientation * im)

                found &=
                    checkbounds(Bool, grid, neg) &&
                    grid[neg] == L &&
                    checkbounds(Bool, grid, pos) &&
                    grid[pos] == L
            end

            total += found
            found && break
        end
    end

    total
end

end  # module
