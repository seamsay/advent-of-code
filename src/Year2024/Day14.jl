module Day14

mutable struct Bot
    r::CartesianIndex{2}
    v::CartesianIndex{2}
end

function load(path)
    open(path) do io
        bots = Bot[]
        while !eof(io)
            rx, ry, vx, vy =
                parse.(
                    Int,
                    match(r"^p=(-?\d+),(-?\d+) v=(-?\d+),(-?\d+)$", readline(io)).captures,
                )
            push!(bots, Bot(CartesianIndex(rx, ry), CartesianIndex(vx, vy)))
        end
        bots
    end
end

function grid(bots, dim = (101, 103))
    g = zeros(Int, dim)
    for b in bots
        g[b.r+CartesianIndex(1, 1)] += 1
    end
    permutedims(g)
end

function run(path, t = 100, dim = (101, 103))
    @assert all(isodd.(dim))

    bots = load(path)
    width, height = dim
    half = Int.((dim .- 1) ./ 2)

    codes = Int[]
    for _ = 1:t
        for b in bots
            nr = b.r + b.v
            x, y = Tuple(nr)

            while x < 0
                x += width
            end

            while x >= width
                x -= width
            end

            while y < 0
                y += height
            end

            while y >= height
                y -= height
            end

            b.r = CartesianIndex(x, y)
        end

        quadrants = zeros(Int, 2, 2)
        for b in bots
            I = Tuple(b.r)
            if any(I .== half)
                continue
            end

            N = map(zip(I, half)) do (i, h)
                if i < h
                    i ÷ h + 1
                elseif i > h
                    (i - 1) ÷ h + 1
                else
                    error()
                end
            end
            quadrants[CartesianIndex(Tuple(N))] += 1
        end

        push!(codes, prod(quadrants))
    end

    codes, bots
end

function part01(path, dim = (101, 103))
    codes, _ = run(path, 100, dim)
    codes[end]
end

end  # module
