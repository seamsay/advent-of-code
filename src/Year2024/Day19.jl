module Day19

using DataStructures: DataStructures


function load(path)
    open(path) do io
        header = readline(io)
        patterns = getindex.(header, findall(r"\w+", header))

        @assert readline(io) == ""

        designs = readlines(io)

        patterns, designs
    end
end

function match_designs(patterns, design, cache)
    if design == ""
        return 1
    end

    if haskey(cache, design)
        return cache[design]
    end

    count = 0
    for pattern in patterns
        if startswith(design, pattern)
            count += match_designs(patterns, design[begin+length(pattern):end], cache)
        end
    end
    cache[design] = count

    count
end

function part01(path)
    patterns, designs = load(path)

    cache = Dict{String,Int}()
    total = 0
    for design in designs
        if match_designs(patterns, design, cache) > 0
            total += 1
        end
    end
    total
end

function part02(path)
    patterns, designs = load(path)

    cache = Dict{String,Int}()
    total = 0
    for design in designs
        total += match_designs(patterns, design, cache)
    end
    total
end

end  # module
