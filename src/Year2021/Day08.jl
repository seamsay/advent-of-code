module Day08

using Underscores: @_

function exactly(collection, count)
    @assert length(collection) == count
    collection
end

function part01(path)
    @_ readlines(path) |>
       split.(__, " | ") |>
       map(split(_[2]), __) |>
       filter.(length(_) in [2, 4, 3, 7], __) |>
       sum(length, __)
end

function part02(path)
    entries = @_ readlines(path) |> split.(__, " | ") |> map(split.(_), __)

    map(entries) do entry
        patterns_str, output_str = entry
        patterns = Set.(patterns_str)

        one = Set(patterns[only(@_ findall(length(_) == 2, patterns))])
        four = Set(patterns[only(@_ findall(length(_) == 4, patterns))])
        seven = Set(patterns[only(@_ findall(length(_) == 3, patterns))])

        all = Set("abcdefg")

        top = setdiff(seven, one)
        top_left = setdiff(four, one)
        top_right = copy(one)
        middle = setdiff(four, one)
        bottom_right = copy(one)
        bottom_left = setdiff(all, four, seven)
        bottom = setdiff(all, four, seven)

        # Top-left is shared with middle.
        # Zero, six, and nine all have six segments and all share top-left.
        # However six and nine have middle, whereas zero does not.
        # Therefore if you intersect each of these with `top_left` or `middle` then only one of them will have one of the segments which will be top-left.
        zero_six_nine = patterns[@_ findall(length(_) == 6, patterns)]
        top_left = @_ map(intersect(_, top_left), zero_six_nine) |>
           filter(length(_) == 1, __) |>
           only(__)
        middle = setdiff(middle, top_left)

        # Same logic for top-right and bottom-right.
        bottom_right = @_ map(intersect(_, bottom_right), zero_six_nine) |>
           filter(length(_) == 1, __) |>
           only(__)
        top_right = setdiff(top_right, bottom_right)

        # Similar logic for bottom-left and bottom, but with two, three, and five.
        two_three_five = patterns[@_ findall(length(_) == 5, patterns)]
        bottom = @_ map(intersect(_, bottom), two_three_five) |>
           filter(length(_) == 1, __) |>
           exactly(__, 2) |>
           first(__)
        bottom_left = setdiff(bottom_left, bottom)

        numbers = Dict(
            union(top, top_left, top_right, bottom_right, bottom_left, bottom) => 0,
            one => 1,
            union(top, top_right, middle, bottom_left, bottom) => 2,
            union(top, top_right, middle, bottom_right, bottom) => 3,
            four => 4,
            union(top, top_left, middle, bottom_right, bottom) => 5,
            union(top, top_left, middle, bottom_right, bottom_left, bottom) => 6,
            seven => 7,
            all => 8,
            union(top, top_left, top_right, middle, bottom_right, bottom) => 9,
        )

        output = Set.(output_str)
        digits = getindex.(Ref(numbers), output)
        evalpoly(10, reverse(digits))
    end |> sum
end

end
