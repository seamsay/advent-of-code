module Day02

module Part01
using Underscores

function displacement(line)
    direction, distance_str = split(line)
    distance = parse(Int, distance_str)

    if direction == "forward"
        [distance, 0]
    elseif direction == "up"
        # Coordinates are position and _depth_, so "up" decreases.
        [0, -distance]
    elseif direction == "down"
        [0, distance]
    else
        error("Unknown direction: $direction")
    end
end

function solution(path)
    @_ readlines(path) |> displacement.(__) |> sum(__) |> prod(__)
end
end

const part01 = Part01.solution

module Part02
using Underscores

struct Position
    horizontal::Int
    depth::Int
end

Position() = Position(0, 0)

struct Submarine
    position::Position
    aim::Int
end

Submarine() = Submarine(Position(), 0)

struct Move
    value::Int
end

struct Aim
    value::Int
end

Base.:(+)(s::Submarine, m::Move) = Submarine(
    Position(s.position.horizontal + m.value, s.position.depth + s.aim * m.value),
    s.aim,
)
Base.:(+)(s::Submarine, a::Aim) = Submarine(s.position, s.aim + a.value)

function decode(line)
    command, value_str = split(line)
    value = parse(Int, value_str)

    if command == "forward"
        Move(value)
    elseif command == "up"
        Aim(-value)
    elseif command == "down"
        Aim(value)
    else
        error("Unknown direction: $direction")
    end
end

function solution(path)
    @_ readlines(path) |>
       sum(decode, __; init = Submarine()) |>
       __.position.horizontal * __.position.depth
end
end

const part02 = Part02.solution

end
