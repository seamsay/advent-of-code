module Day01

using Underscores

function part01(path)
    @_ readlines(path) |> parse.(Int, __) |> diff |> sum(__ .> 0)
end

function part02(path)
    @_ readlines(path) |>
       parse.(Int, __) |>
       zip(__[begin:end-2], __[begin+1:end-1], __[begin+2:end]) |>
       sum.(__) |>
       diff |>
       sum(__ .> 0)
end

end
