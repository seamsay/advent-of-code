module Day12

using Underscores: @_

function load(path)
    edges = Dict{String,Vector{String}}()
    for line in readlines(path)
        from, to = match(r"([A-Za-z]+)-([A-Za-z]+)", line).captures

        if !haskey(edges, from)
            edges[from] = []
        end
        if !haskey(edges, to)
            edges[to] = []
        end

        push!(edges[from], to)
        push!(edges[to], from)
    end

    @assert haskey(edges, "start")
    @assert haskey(edges, "end")

    edges
end

alllowercase(string) = all(islowercase.(Char.(codeunits(string))))

function run(path, disallowed)
    graph = load(path)

    stack = [["start"]]
    paths = Vector{String}[]
    while !isempty(stack)
        nodes = pop!(stack)
        possibilities = graph[nodes[end]]
        for possibility in possibilities
            if possibility == "start" || disallowed(possibility, nodes)
                continue
            end

            new_nodes = copy(nodes)
            push!(new_nodes, possibility)

            if possibility == "end"
                push!(paths, new_nodes)
            else
                push!(stack, new_nodes)
            end
        end
    end

    length(paths)
end

part01(path) = run(path, (p, ns) -> (alllowercase(p) && any(@_ map(_ == p, ns))))

part02(path) = run(path, function (p, ns)
    function add!(counts, n)
        if !alllowercase(n)
            return
        end

        if !haskey(counts, n)
            counts[n] = 0
        end

        counts[n] += 1
    end

    counts = Dict{String,Int}()

    for n in ns
        add!(counts, n)
    end

    add!(counts, p)

    @_(sum(_ >= 2, values(counts)) >= 2) || @_(any(_ > 2, values(counts)))
end)

end
