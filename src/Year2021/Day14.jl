module Day14

using Underscores: @_

struct WindowIterator{I}
    iterator::I
    n::Int
end

Base.IteratorEltype(::Type{WindowIterator{I}}) where {I} = Base.IteratorEltype(I)
Base.eltype(::Type{WindowIterator{I}}) where {I} = Vector{eltype(I)}

window_iteratorsize(w) = Base.HasLength()
window_iteratorsize(::Base.IsInfinite) = Base.IsInfinite()
window_iteratorsize(::Base.SizeUnknown) = Base.SizeUnknown()
Base.IteratorSize(::Type{WindowIterator{I}}) where {I} =
    window_iteratorsize(Base.IteratorSize(I))
Base.length(w::WindowIterator) = length(w.iterator) - (w.n - 1)

# TODO: There is a type instability in this function, can we get rid of it by redesigning the iterator?
# TODO: There's got to be a better way...
function Base.iterate(w::WindowIterator)
    window = eltype(w)(undef, w.n)

    i = 0
    next = iterate(w.iterator)
    while next !== nothing && i != w.n
        i += 1
        item, state = next
        window[i] = item
        next = iterate(w.iterator, state)
    end

    if i == w.n
        (window, (window[2:end], next))
    else
        error("Iterator could not fill a single window.")
    end
end

function Base.iterate(w::WindowIterator, state)
    window, next = state
    if next === nothing
        nothing
    else
        item, inner_state = next
        push!(window, item)
        (window, (window[2:end], iterate(w.iterator, inner_state)))
    end
end

function load(path)
    lines = readlines(path)

    template = first(lines)

    @assert lines[2] == ""

    rules = @_ lines[3:end] |>
       split.(__, " -> ") |>
       map(collect(_[1]) => _[2][1], __) |>
       Dict(__)

    template, rules
end

function count(iterator)
    counts = Dict{eltype(iterator),BigInt}()

    for key in iterator
        if !haskey(counts, key)
            counts[key] = big(0)
        end
        counts[key] += 1
    end

    counts
end

function run(path, steps)
    template, rules = load(path)
    chars = collect(template)
    first_char = first(chars)
    last_char = last(chars)

    pairs = count(WindowIterator(chars, 2))
    for _ = 1:steps
        new_pairs = empty(pairs)
        for (pair, frequency) in pairs
            if haskey(rules, pair)
                inner = rules[pair]
                left, right = pair

                left_pair = [left, inner]
                if !haskey(new_pairs, left_pair)
                    new_pairs[left_pair] = 0
                end
                new_pairs[left_pair] += frequency

                right_pair = [inner, right]
                if !haskey(new_pairs, right_pair)
                    new_pairs[right_pair] = 0
                end
                new_pairs[right_pair] += frequency
            else
                if !haskey(new_pairs, pair)
                    new_pairs[pair] = 0
                end
                new_pairs[pair] += frequency
            end
        end
        pairs = new_pairs
    end

    frequencies = Dict{Char,BigInt}()
    for ((left, right), frequency) in pairs
        if !haskey(frequencies, left)
            frequencies[left] = 0
        end
        frequencies[left] += frequency

        if !haskey(frequencies, right)
            frequencies[right] = 0
        end
        frequencies[right] += frequency
    end

    # Every character appears in exactly two pairs, except the first and last which only appear in one.
    # This means that we need to divide every frequency by two, but only after adding one to the first and last.
    frequencies[first_char] += 1
    frequencies[last_char] += 1
    frequencies = Dict(char => div(frequency, 2) for (char, frequency) in frequencies)

    maximum(values(frequencies)) - minimum(values(frequencies))
end

part01(path) = run(path, 10)

part02(path) = run(path, 40)

end
