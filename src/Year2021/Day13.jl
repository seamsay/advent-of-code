module Day13

using Underscores: @_

function load(path)
    lines = eachline(path)

    dots = Vector{Int}[]
    for line in lines
        if line == ""
            break
        end
        x, y = @_ split(line, ',') |> parse.(Int, __)
        push!(dots, [x, y])
    end

    folds = Union{Vertical,Horizontal}[]
    for line in lines
        coordinate, value = match(r"fold along ([xy])=(\d+)", line).captures
        type = if coordinate == "x"
            Vertical
        elseif coordinate == "y"
            Horizontal
        else
            error("Coordinate '$coordinate' not recognised!")
        end
        push!(folds, type(parse(Int, value)))
    end

    dots, folds
end

struct Vertical
    x::Int
end

struct Horizontal
    y::Int
end

reflect(coordinate, line::Vertical) =
    let (x, y) = coordinate, l = line.x
        [l - abs(x - l), y]
    end

reflect(coordinate, line::Horizontal) =
    let (x, y) = coordinate, l = line.y
        [x, l - abs(y - l)]
    end

function fold(path, maximum)
    dots, folds = load(path)

    for fold in Iterators.take(folds, maximum)
        dots = reflect.(dots, Ref(fold)) |> unique
    end

    dots
end

# TODO: Can we get some proper OCR in here?
const GRIDS_TO_LETTERS = Dict(
    [
        false true true false
        true false false true
        true false false true
        true true true true
        true false false true
        true false false true
    ] => 'A',
    [
        true true true true
        true false false false
        true true true false
        true false false false
        true false false false
        true true true true
    ] => 'E',
    [
        true true true true
        true false false false
        true true true false
        true false false false
        true false false false
        true false false false
    ] => 'F',
    [
        true false false true
        true false true false
        true true false false
        true false true false
        true false true false
        true false false true
    ] => 'K',
    [
        true false false false
        true false false false
        true false false false
        true false false false
        true false false false
        true true true true
    ] => 'L',
    [
        true true true false
        true false false true
        true false false true
        true true true false
        true false true false
        true false false true
    ] => 'R',
    [
        true false false true
        true false false true
        true false false true
        true false false true
        true false false true
        false true true false
    ] => 'U',
    # NOTE: The O is 5x5, all other letters are 6x4.
    [
        true true true true true
        true false false false true
        true false false false true
        true false false false true
        true true true true true
    ] => 'O',
)

part01(path) = length(fold(path, 1))

function part02(path)
    dots = fold(path, typemax(Int))
    # TODO: Why do I need to swap the coordinates?
    indices =
        @_ map([_[2], _[1]], dots) |> map(_ .+ 1, __) |> Tuple.(__) |> CartesianIndex.(__)

    grid = fill(false, Tuple(maximum(indices)))
    grid[indices] .= true

    splits = findall(!any, eachcol(grid))
    letters = Matrix{Bool}[]
    previous = 1
    for index in splits
        push!(letters, grid[:, previous:index-1])
        previous = index + 1
    end
    push!(letters, grid[:, previous:end])

    characters = @_ map(GRIDS_TO_LETTERS[_], letters)

    join(characters)
end

end
