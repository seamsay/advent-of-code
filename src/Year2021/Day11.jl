module Day11

using Underscores: @_

const FLASH_AT = 10

load(path) = @_ readlines(path) |> split.(__, "") |> hcat(__...) |> parse.(Int, __) |> __'

function part01(path)
    grid = load(path)
    flashes = 0

    for _ = 1:100
        grid .+= 1

        flashed = fill(false, size(grid))
        stack = findall(==(FLASH_AT), grid)
        while !isempty(stack)
            index = pop!(stack)
            if flashed[index]
                continue
            end
            flashed[index] = true
            flashes += 1

            for shift in
                CartesianIndex.([
                (-1, -1),
                (-1, 0),
                (-1, 1),
                (0, -1),
                (0, 1),
                (1, -1),
                (1, 0),
                (1, 1),
            ])
                adjacent = index + shift
                if !checkbounds(Bool, grid, adjacent)
                    continue
                end

                grid[adjacent] += 1

                if grid[adjacent] >= FLASH_AT
                    push!(stack, adjacent)
                end
            end
        end

        grid[flashed] .= 0
    end

    flashes
end

function part02(path)
    grid = load(path)
    flashes = 0

    for step in Iterators.countfrom(1)
        grid .+= 1

        flashed = fill(false, size(grid))
        stack = findall(==(FLASH_AT), grid)
        while !isempty(stack)
            index = pop!(stack)
            if flashed[index]
                continue
            end
            flashed[index] = true
            flashes += 1

            for shift in
                CartesianIndex.([
                (-1, -1),
                (-1, 0),
                (-1, 1),
                (0, -1),
                (0, 1),
                (1, -1),
                (1, 0),
                (1, 1),
            ])
                adjacent = index + shift
                if !checkbounds(Bool, grid, adjacent)
                    continue
                end

                grid[adjacent] += 1

                if grid[adjacent] >= FLASH_AT
                    push!(stack, adjacent)
                end
            end
        end

        grid[flashed] .= 0

        if all(flashed)
            return step
        end
    end
end

end
