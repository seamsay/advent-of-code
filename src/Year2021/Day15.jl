module Day15

import DataStructures
using Underscores: @_

load(path) = @_ readlines(path) |> split.(__, "") |> hcat(__...) |> parse.(Int, __)

function run(grid)
    queue = DataStructures.PriorityQueue{Tuple{Int,Int},Int}()
    DataStructures.enqueue!(queue, (1, 1), 0)
    seen = Set{Tuple{Int,Int}}()
    while !isempty(queue)
        (row, column), sum = DataStructures.dequeue_pair!(queue)
        if row == lastindex(grid, 1) && column == lastindex(grid, 2)
            return sum
        end
        push!(seen, (row, column))

        up_row = row - 1
        if checkbounds(Bool, grid, up_row, column)
            up_sum = sum + grid[up_row, column]
            key = (up_row, column)
            if !in((up_row, column), seen) && get(queue, key, typemax(Int)) > up_sum
                queue[key] = up_sum
            end
        end

        down_row = row + 1
        if checkbounds(Bool, grid, down_row, column)
            down_sum = sum + grid[down_row, column]
            key = (down_row, column)
            if !in((down_row, column), seen) && get(queue, key, typemax(Int)) > down_sum
                queue[key] = down_sum
            end
        end

        left_column = column - 1
        if checkbounds(Bool, grid, row, left_column)
            left_sum = sum + grid[row, left_column]
            key = (row, left_column)
            if !in((row, left_column), seen) && get(queue, key, typemax(Int)) > left_sum
                queue[key] = left_sum
            end
        end

        right_column = column + 1
        if checkbounds(Bool, grid, row, right_column)
            right_sum = sum + grid[row, right_column]
            key = (row, right_column)
            if !in((row, right_column), seen) && get(queue, key, typemax(Int)) > right_sum
                queue[key] = right_sum
            end
        end
    end

    error("I don't even think this can be hit.")
end

function part01(path)
    grid = load(path)
    run(grid)
end

function part02(path)
    mini = load(path)

    inc(x) = x % 9 + 1
    rows, columns = size(mini)
    grid = zeros(Int, rows * 5, columns * 5)
    grid[1:rows, 1:columns] = mini
    for r = 2:5
        grid[rows*(r-1)+1:rows*r, 1:columns] .=
            inc.(grid[rows*(r-2)+1:rows*(r-1), 1:columns])
    end
    for c = 2:5
        grid[1:rows, columns*(c-1)+1:columns*c] .=
            inc.(grid[1:rows, columns*(c-2)+1:columns*(c-1)])
        for r = 2:5
            grid[rows*(r-1)+1:rows*r, columns*(c-1)+1:columns*c] .=
                inc.(grid[rows*(r-2)+1:rows*(r-1), columns*(c-1)+1:columns*c])
        end
    end

    run(grid)
end

end
