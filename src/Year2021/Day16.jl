module Day16

using Underscores: @_

macro newtype(name, inner)
    quote
        struct $name
            value::$inner
        end

        Base.getindex(x::$name) = x.value
    end
end

macro operator(name)
    quote
        struct $name <: Operator
            subpackets::PacketList
        end
    end
end

load(path) = @_ read(path, String) |>
   chomp(__) |>
   hex2bytes(__) |>
   digits.(__; base = 2, pad = 8) |>
   reverse.(__) |>
   Iterators.flatten(__) |>
   UInt8.(__)

@newtype Version Int
@newtype TypeID Int
@newtype Literal Int

struct Packet{T}
    version::Version
    value::T
end

@newtype PacketList Vector{Packet}

Base.iterate(l::PacketList) = iterate(l[])
Base.iterate(l::PacketList, state) = iterate(l[], state)

abstract type Operator end

@operator Sum
@operator Product
@operator Minimum
@operator Maximum
@operator Greater
@operator Less
@operator Equal

parse!(::Type{Version}, io) = evalpoly(2, reverse(read(io, 3))) |> Version
parse!(::Type{TypeID}, io) = evalpoly(2, reverse(read(io, 3))) |> TypeID

function parse!(::Type{Literal}, io)
    bits = UInt8[]
    while true
        finished = !read(io, Bool)
        append!(bits, read(io, 4))
        finished && break
    end

    evalpoly(2, reverse(bits)) |> Literal
end

function parse!(::Type{Operator}, id, io)
    list_type = only(read(io, 1))
    subpackets = if list_type == 0
        n_bits = evalpoly(2, reverse(read(io, 15)))
        data = IOBuffer(read(io, n_bits))

        subpackets = PacketList([])
        while !eof(data)
            push!(subpackets[], parse!(Packet, data))
        end

        subpackets
    elseif list_type == 1
        n_subpackets = evalpoly(2, reverse(read(io, 11)))

        subpackets = PacketList([])
        for _ = 1:n_subpackets
            push!(subpackets[], parse!(Packet, io))
        end

        subpackets
    else
        error("List type '$list_type' not recognised...")
    end

    type = if id == 0
        Sum
    elseif id == 1
        Product
    elseif id == 2
        Minimum
    elseif id == 3
        Maximum
    elseif id == 4
        # TODO: It probably would have been neater to handle the Literal here...
        error("Literal should have already been handled!")
    elseif id == 5
        Greater
    elseif id == 6
        Less
    elseif id == 7
        Equal
    end

    type(subpackets)
end

function parse!(::Type{Packet}, io)
    version = parse!(Version, io)
    type = parse!(TypeID, io)

    value = if type[] == 4
        parse!(Literal, io)
    else
        parse!(Operator, type[], io)
    end

    Packet(version, value)
end

function part01(path)
    io = IOBuffer(load(path))
    root = parse!(Packet, io)

    versions(p::Packet{Literal}) = [p.version]
    versions(p::Packet{<:Operator}) = append!([p.version], versions(p.value))
    versions(o::Operator) = versions(o.subpackets)
    versions(l::PacketList) = Iterators.flatten(versions(p) for p in l[])

    sum(getindex, versions(root))
end

function part02(path)
    io = IOBuffer(load(path))
    root = parse!(Packet, io)

    evaluate(p::Packet) = evaluate(p.value)
    evaluate(l::Literal) = l[]
    evaluate(s::Sum) = sum(evaluate, s.subpackets)
    evaluate(p::Product) = prod(evaluate, p.subpackets)
    evaluate(m::Minimum) = minimum(evaluate, m.subpackets)
    evaluate(m::Maximum) = maximum(evaluate, m.subpackets)
    evaluate(g::Greater) = Int(evaluate(g.subpackets[][1]) > evaluate(g.subpackets[][2]))
    evaluate(l::Less) = Int(evaluate(l.subpackets[][1]) < evaluate(l.subpackets[][2]))
    evaluate(e::Equal) = Int(evaluate(e.subpackets[][1]) == evaluate(e.subpackets[][2]))

    evaluate(root)
end

end
