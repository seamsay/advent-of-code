module Day07

using Statistics: mean, median
using Underscores: @_

function run(path, statistic, fuel)
    positions = @_ read(path, String) |> split(__, ',') |> parse.(Int, __)

    best_float = statistic(positions)
    best_up = ceil(Int, best_float)
    best_down = floor(Int, best_float)

    minimum(fuel.(Ref(positions), [best_up, best_down]))
end

# To derive, differentiate the fuel equation and set to zero (normal minimisation) then rearrange to find the median and something close to the mean.

# Fuel used is absolute deviation, median minimises the absolute deviation.
part01(path) = run(path, median, (ps, b) -> sum(abs.(ps .- b)))

# Fuel used is proportional `d^2 + d` where `d` is the absolute deviation, so mean will get you to within 0.5 of the best position.
part02(path) = run(path, mean, (ps, b) -> let n = abs.(ps .- b)
    Int(sum(n .* (n .+ 1)) / 2)
end)

end
