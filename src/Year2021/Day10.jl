module Day10

using Statistics: median
using Underscores: @_

const MATCHING = Dict('(' => ')', '[' => ']', '{' => '}', '<' => '>')

const REVERSE = Dict(@_ map(_[1] => _[2], zip(values(MATCHING), keys(MATCHING))))

function parse(line)
    stack = Char[]
    for char in line
        if char in keys(MATCHING)
            push!(stack, char)
        elseif MATCHING[pop!(stack)] != char
            return char
        elseif !in(char, values(MATCHING))
            error("Unknown character '$char'!")
        end
    end

    stack
end

function part01(path)
    scores = Dict(')' => 3, ']' => 57, '}' => 1197, '>' => 25137)

    @_ readlines(path) |>
       parse.(__) |>
       filter(isa(_, Char), __) |>
       getindex.(Ref(scores), __) |>
       sum(__)
end

function part02(path)
    scores = Dict('(' => 1, '[' => 2, '{' => 3, '<' => 4)

    @_ readlines(path) |>
       parse.(__) |>
       filter(!isa(_, Char), __) |>
       map(getindex.(Ref(scores), _), __) |>
       reverse.(__) |>
       reduce.(5 * _1 + _2, __; init = 0) |>
       sort(__) |>
       median(__) |>
       Int(__)
end

end
