module Day03

using Underscores

bits2int(bits) = sum(bits .* 2 .^ reverse(0:length(bits)-1))

function part01(path)
    # NOTE: This is waaaaaayyyyyy too clever to ever be used seriously.
    @_ readlines(path) |>
       split.(__, "") |>
       map(parse.(Int, _), __) |>
       reduce(_1 .+ (-1) .^ _2, __) |>
       bits2int(clamp.(__, 0, 1)) * bits2int(clamp.(-__, 0, 1)) |>
       Int
end

function filter_by_common(array, modifier)
    bits = length(first(array))
    @assert @_ all(length(_) == bits, array)

    for i = 1:bits
        common = Day03.common(array, modifier)
        array = @_ filter(_[i] == common[i], array)
        if length(array) == 1
            return reverse(only(array))
        end
    end

    error("Array was not fully filtered down: $array")
end

# After the reduce: negative means 1 is more common, positive means 0 is more common, and zero means they are equally common.
function common(array, modifier)
    bits = length(first(array))
    @assert @_ all(length(_) == bits, array)

    @_ array |>
       reduce(_1 .+ (-1) .^ _2, __; init = zeros(Int, bits)) |>
       clamp.(modifier.(__), 0, 1)
end

function part02(path)
    bits = @_ readlines(path) |> split.(__, "") |> map(parse.(Int, _), __)

    # For most common break ties as 1, so 0 after reduce should become 1.
    o2_gen_rating = filter_by_common(bits, b -> -b + 1)
    # For least common break ties as 0, so 0 after reduce should stay 0.
    co2_scrub_rating = filter_by_common(bits, identity)

    evalpoly(2, o2_gen_rating) * evalpoly(2, co2_scrub_rating)
end

end
