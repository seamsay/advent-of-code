module Day04

import DelimitedFiles
using Underscores: @_

struct Element
    value::Int
    marked::Bool
end

Element(value) = Element(value, false)

Base.show(io::IO, e::Element) =
    if e.marked
        print(io, "x", e.value)
    else
        print(io, " ", e.value)
    end

mark(element, value) = Element(element.value, element.marked || element.value == value)

check(board) =
    any(@_ mapslices(all(e.marked for e in _), board; dims = 1)) ||
    any(@_ mapslices(all(e.marked for e in _), board; dims = 2)[:, 1])

function parse_input(io)
    called_str, boards_str... = split(read(io, String), "\n\n")

    called = DelimitedFiles.readdlm(IOBuffer(called_str), ',', Int)[1, :]
    boards = @_ DelimitedFiles.readdlm.(IOBuffer.(boards_str), Int) |> map(Element.(_), __)

    called, boards
end

function part01(path)
    called, boards = open(parse_input, path)

    for value in called
        boards = @_ map(mark.(_, value), boards)

        winners = check.(boards)
        winner = findfirst(winners)

        if !isnothing(winner)
            return value * @_ boards[winner] |> filter(!_.marked, __) |> sum(_.value, __)
        end
    end

    error("No board won!")
end

function part02(path)
    called, boards = open(parse_input, path)

    for value in called
        boards = @_ map(mark.(_, value), boards)
        playing = @_ filter(!check, boards)

        if isempty(playing)
            return value * @_ only(boards) |> filter(!_.marked, __) |> sum(_.value, __)
        end

        boards = playing
    end

    error("Some boards never lost!")
end

end
