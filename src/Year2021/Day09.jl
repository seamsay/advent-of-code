module Day09

using Underscores: @_

load(path) = @_ readlines(path) |> split.(__, "") |> hcat(__...) |> parse.(Int, __) |> __'

function depths(grid)
    points = CartesianIndex{2}[]
    rows, columns = size(grid)
    for index in eachindex(grid)
        row, column = Tuple(index)
        cell = grid[row, column]

        up_higher = row == 1 || cell < grid[row-1, column]
        down_higher = row == rows || cell < grid[row+1, column]
        left_higher = column == 1 || cell < grid[row, column-1]
        right_higher = column == columns || cell < grid[row, column+1]

        if up_higher && down_higher && left_higher && right_higher
            push!(points, index)
        end
    end

    points
end

function part01(path)
    grid = load(path)
    depths = Day09.depths(grid)
    sum(grid[depths] .+ 1)
end

function part02(path)
    grid = load(path)
    rows, columns = size(grid)
    depths = Day09.depths(grid)

    basins = Int[]
    visited = fill(false, rows, columns)
    for depth in depths
        size = 0

        stack = [depth]
        while !isempty(stack)
            index = pop!(stack)
            if grid[index] == 9 || visited[index]
                continue
            end
            visited[index] = true

            size += 1

            row, column = Tuple(index)

            if row != 1
                push!(stack, CartesianIndex(row - 1, column))
            end

            if row != rows
                push!(stack, CartesianIndex(row + 1, column))
            end

            if column != 1
                push!(stack, CartesianIndex(row, column - 1))
            end

            if column != columns
                push!(stack, CartesianIndex(row, column + 1))
            end
        end

        push!(basins, size)
    end

    @_ sort(basins; rev = true) |> Iterators.take(__, 3) |> prod(__)
end

end
