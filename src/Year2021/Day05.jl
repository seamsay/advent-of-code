module Day05

using Underscores: @_

parse(path) = @_ readlines(path) |>
   match.(r"(\d+),(\d+) -> (\d+),(\d+)", __) |>
   getproperty.(__, :captures) |>
   map(Base.parse.(Int, _), __) |>
   # Data file is 0-based, but Julia 1-based.
   map(_ .+ 1, __) |>
   # y-coordinate is row, x-coordinate is column.
   map(CartesianIndex(_[2], _[1]) => CartesianIndex(_[4], _[3]), __)

extent(lines) = maximum.(lines) |> maximum

function part01(path)
    lines = @_ parse(path) |> filter(_[1][1] == _[2][1] || _[1][2] == _[2][2], __)

    grid = zeros(Int, extent(lines).I)
    for (from, to) in lines
        (from, to) = if from > to
            (to, from)
        else
            (from, to)
        end

        grid[from:to] .+= 1
    end

    sum(grid .>= 2)
end

function part02(path)
    lines = parse(path)
    for (from, to) in lines
        @assert from[1] == to[1] ||
                from[2] == to[2] ||
                abs(to[1] - from[1]) == abs(to[2] - from[2])
    end

    grid = zeros(Int, extent(lines).I)
    for (from, to) in lines
        xs = if from[1] == to[1]
            repeat([from[1]], abs(from[2] - to[2]) + 1)
        else
            range(from[1], to[1], step = sign(to[1] - from[1]))
        end

        ys = if from[2] == to[2]
            repeat([from[2]], abs(from[1] - to[1]) + 1)
        else
            range(from[2], to[2], step = sign(to[2] - from[2]))
        end

        for (x, y) in zip(xs, ys)
            grid[x, y] += 1
        end
    end

    sum(grid .>= 2)
end

end
