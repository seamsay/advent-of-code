module Day06

using Underscores: @_

function run(path, days)
    ages = @_ read(path, String) |> split(__, ',') |> parse.(Int, __)

    fish = Dict(0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0)
    for age in ages
        fish[age] += 1
    end

    for _ = 1:days
        children = fish[0]

        for age = 1:8
            fish[age-1] = fish[age]
        end

        fish[6] += children
        fish[8] = children
    end

    sum(values(fish))
end

part01(path) = run(path, 80)

part02(path) = run(path, 256)

end
