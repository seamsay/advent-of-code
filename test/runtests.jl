using AdventOfCode
using Test

using Aqua: Aqua
using JuliaFormatter: JuliaFormatter

macro file(paths...)
    dir = dirname(String(__source__.file))
    :(joinpath($dir, $(paths...)))
end

@testset "Advent Of Code" begin
    @testset "Linting" begin
        @testset "Code Format (JuliaFormatter.jl)" begin
            @test JuliaFormatter.format(dirname(@__DIR__); overwrite = false)
        end

        @testset "Code Quality (Aqua.jl)" begin
            Aqua.test_all(AdventOfCode)
        end
    end

    @testset "Solutions" begin
        @testset "Year 2021" begin
            include("Year2021/runtests.jl")
        end

        @testset "Year 2022" begin
            include("Year2022/runtests.jl")
        end

        @testset "Year 2023" begin
            include("Year2023/runtests.jl")
        end

        @testset "Year 2024" begin
            include("Year2024/runtests.jl")
        end
    end
end
