@testset "Day 01" begin
    @testset "Part 01" begin
        @test Year2023.Day01.part01(@file("Day01", "test01.txt")) == 142
        @test Year2023.Day01.part01(@file("Day01", "input.txt")) == 55477
    end

    @testset "Part 02" begin
        @test Year2023.Day01.part02(@file("Day01", "test02.txt")) == 281
        @test Year2023.Day01.part02(@file("Day01", "input.txt")) == 54431
    end
end

@testset "Day 02" begin
    @testset "Part 01" begin
        @test Year2023.Day02.part01(@file("Day02", "test.txt")) == 8
        @test Year2023.Day02.part01(@file("Day02", "input.txt")) == 2545
    end

    @testset "Part 02" begin
        @test Year2023.Day02.part02(@file("Day02", "test.txt")) == 2286
        @test Year2023.Day02.part02(@file("Day02", "input.txt")) == 78111
    end
end

@testset "Day 03" begin
    @testset "Part 01" begin
        @test Year2023.Day03.part01(@file("Day03", "test.txt")) == 4361
        @test Year2023.Day03.part01(@file("Day03", "input.txt")) == 521601
    end

    @testset "Part 02" begin
        @test Year2023.Day03.part02(@file("Day03", "test.txt")) == 467835
        @test Year2023.Day03.part02(@file("Day03", "input.txt")) == 80694070
    end
end

@testset "Day 04" begin
    @testset "Part 01" begin
        @test Year2023.Day04.part01(@file("Day04", "test.txt")) == 13
        @test Year2023.Day04.part01(@file("Day04", "input.txt")) == 21919
    end

    @testset "Part 02" begin
        @test Year2023.Day04.part02(@file("Day04", "test.txt")) == 30
        @test Year2023.Day04.part02(@file("Day04", "input.txt")) == 9881048
    end
end

@testset "Day 05" begin
    @testset "Part 01" begin
        @test Year2023.Day05.part01(@file("Day05", "test.txt")) == 35
        @test Year2023.Day05.part01(@file("Day05", "input.txt")) == 309796150
    end

    @testset "Part 02" begin
        @test Year2023.Day05.part02(@file("Day05", "test.txt")) == 46
        @test Year2023.Day05.part02(@file("Day05", "input.txt")) == 50716416
    end
end

@testset "Day 06" begin
    @testset "Part 01" begin
        @test Year2023.Day06.part01(@file("Day06", "test.txt")) == 288
        @test Year2023.Day06.part01(@file("Day06", "input.txt")) == 2449062
    end

    @testset "Part 02" begin
        @test Year2023.Day06.part02(@file("Day06", "test.txt")) == 71503
        @test Year2023.Day06.part02(@file("Day06", "input.txt")) == 33149631
    end
end

@testset "Day 07" begin
    @testset "Part 01" begin
        @test Year2023.Day07.part01(@file("Day07", "test.txt")) == 6440
        @test Year2023.Day07.part01(@file("Day07", "input.txt")) == 250347426
    end

    @testset "Part 02" begin
        @test Year2023.Day07.part02(@file("Day07", "test.txt")) == 5905
        @test Year2023.Day07.part02(@file("Day07", "input.txt")) == 251224870
    end
end

@testset "Day 08" begin
    @testset "Part 01" begin
        @test Year2023.Day08.part01(@file("Day08", "test01.txt")) == 2
        @test Year2023.Day08.part01(@file("Day08", "test02.txt")) == 6
        @test Year2023.Day08.part01(@file("Day08", "input.txt")) == 14681
    end

    @testset "Part 02" begin
        @test Year2023.Day08.part02(@file("Day08", "test03.txt")) == 6
        @test Year2023.Day08.part02(@file("Day08", "input.txt")) == 14321394058031
    end
end

@testset "Day 09" begin
    @testset "Part 01" begin
        @test Year2023.Day09.part01(@file("Day09", "test.txt")) == 114
        @test Year2023.Day09.part01(@file("Day09", "input.txt")) == 1637452029
    end

    @testset "Part 02" begin
        @test Year2023.Day09.part02(@file("Day09", "test.txt")) == 2
        @test Year2023.Day09.part02(@file("Day09", "input.txt")) == 908
    end
end

@testset "Day 10" begin
    @testset "Part 01" begin
        @test Year2023.Day10.part01(@file("Day10", "test01.txt")) == 4
        @test Year2023.Day10.part01(@file("Day10", "test02.txt")) == 8
        @test Year2023.Day10.part01(@file("Day10", "test03.txt")) == 22
        @test Year2023.Day10.part01(@file("Day10", "test04.txt")) == 70
        @test Year2023.Day10.part01(@file("Day10", "test05.txt")) == 80
        @test Year2023.Day10.part01(@file("Day10", "input.txt")) == 6886
    end

    @testset "Part 02" begin
        @test Year2023.Day10.part02(@file("Day10", "test01.txt")) == 1
        @test Year2023.Day10.part02(@file("Day10", "test02.txt")) == 1
        @test Year2023.Day10.part02(@file("Day10", "test03.txt")) == 4
        @test Year2023.Day10.part02(@file("Day10", "test04.txt")) == 8
        @test Year2023.Day10.part02(@file("Day10", "test05.txt")) == 10
        @test Year2023.Day10.part02(@file("Day10", "input.txt")) == 371
    end
end

@testset "Day 11" begin
    @testset "Part 01" begin
        @test Year2023.Day11.part01(@file("Day11", "test.txt")) == 374
        @test Year2023.Day11.part01(@file("Day11", "input.txt")) == 10490062
    end

    @testset "Part 02" begin
        @test Year2023.Day11.part02(@file("Day11", "test.txt"), 2) == 374
        @test Year2023.Day11.part02(@file("Day11", "test.txt"), 10) == 1030
        @test Year2023.Day11.part02(@file("Day11", "test.txt"), 100) == 8410
        @test Year2023.Day11.part02(@file("Day11", "input.txt"), 2) == 10490062
        @test Year2023.Day11.part02(@file("Day11", "input.txt"), 1_000_000) == 382979724122
    end
end

@testset "Day 12" begin
    @testset "Part 01" begin
        @test Year2023.Day12.part01(@file("Day12", "test.txt")) == 21
        @test Year2023.Day12.part01(@file("Day12", "input.txt")) == 6871
    end

    @testset "Part 02" begin
        @test Year2023.Day12.part02(@file("Day12", "test.txt")) == 525152
        @test Year2023.Day12.part02(@file("Day12", "input.txt")) == 2043098029844
    end
end

@testset "Day 13" begin
    @testset "Part 01" begin
        @test Year2023.Day13.part01(@file("Day13", "test.txt")) == 405
        @test Year2023.Day13.part01(@file("Day13", "input.txt")) == 29130
    end

    @testset "Part 02" begin
        @test Year2023.Day13.part02(@file("Day13", "test.txt")) == 400
        @test Year2023.Day13.part02(@file("Day13", "input.txt")) == 33438
    end
end

@testset "Day 14" begin
    @testset "Part 01" begin
        @test Year2023.Day14.part01(@file("Day14", "test.txt")) == 136
        @test Year2023.Day14.part01(@file("Day14", "input.txt")) == 110677
    end

    @testset "Part 02" begin
        @test Year2023.Day14.part02(@file("Day14", "test.txt")) == 64
        @test Year2023.Day14.part02(@file("Day14", "input.txt")) == 90551
    end
end

@testset "Day 15" begin
    @testset "Part 01" begin
        @test Year2023.Day15.part01(@file("Day15", "test.txt")) == 1320
        @test Year2023.Day15.part01(@file("Day15", "input.txt")) == 515974
    end

    @testset "Part 02" begin
        @test Year2023.Day15.part02(@file("Day15", "test.txt")) == 145
        @test Year2023.Day15.part02(@file("Day15", "input.txt")) == 265894
    end
end

@testset "Day 16" begin
    @testset "Part 01" begin
        @test Year2023.Day16.part01(@file("Day16", "test.txt")) == 46
        @test Year2023.Day16.part01(@file("Day16", "input.txt")) == 6906
    end

    @testset "Part 02" begin
        @test Year2023.Day16.part02(@file("Day16", "test.txt")) == 51
        @test Year2023.Day16.part02(@file("Day16", "input.txt")) == 7330
    end
end

@testset "Day 17" begin
    @testset "Part 01" begin
        @test Year2023.Day17.part01(@file("Day17", "test.txt")) == 102
        @test Year2023.Day17.part01(@file("Day17", "input.txt")) == 1260
    end

    @testset "Part 02" begin
        @test Year2023.Day17.part02(@file("Day17", "test.txt")) == 94
        @test Year2023.Day17.part02(@file("Day17", "input.txt")) == 1416
    end
end

@testset "Day 18" begin
    @testset "Part 01" begin
        @test Year2023.Day18.part01(@file("Day18", "test.txt")) == 62
        @test Year2023.Day18.part01(@file("Day18", "input.txt")) == 52231
    end

    @testset "Part 02" begin
        @test Year2023.Day18.part02(@file("Day18", "test.txt")) == 952408144115
        @test Year2023.Day18.part02(@file("Day18", "input.txt")) == 57196493937398
    end
end

@testset "Day 19" begin
    @testset "Part 01" begin
        @test Year2023.Day19.part01(@file("Day19", "test.txt")) == 19114
        @test Year2023.Day19.part01(@file("Day19", "input.txt")) == 434147
    end

    @testset "Part 02" begin
        @test Year2023.Day19.part02(@file("Day19", "test.txt")) == 167409079868000
        @test Year2023.Day19.part02(@file("Day19", "input.txt")) == 136146366355609
    end
end
