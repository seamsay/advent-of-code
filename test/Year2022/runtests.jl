@testset "Year 2022" begin
    @testset "Day 01" begin
        @testset "Part 01" begin
            @test Year2022.Day01.part01(@file("Day01", "test.txt")) == 24000
            @test Year2022.Day01.part01(@file("Day01", "input.txt")) == 66616
        end

        @testset "Part 02" begin
            @test Year2022.Day01.part02(@file("Day01", "test.txt")) == 45000
            @test Year2022.Day01.part02(@file("Day01", "input.txt")) == 199172
        end
    end

    @testset "Day 02" begin
        @testset "Part 01" begin
            @test Year2022.Day02.part01(@file("Day02", "test.txt")) == 15
            @test Year2022.Day02.part01(@file("Day02", "input.txt")) == 10941
        end

        @testset "Part 02" begin
            @test Year2022.Day02.part02(@file("Day02", "test.txt")) == 12
            @test Year2022.Day02.part02(@file("Day02", "input.txt")) == 13071
        end
    end

    @testset "Day 03" begin
        @testset "Part 01" begin
            @test Year2022.Day03.part01(@file("Day03", "test.txt")) == 157
            @test Year2022.Day03.part01(@file("Day03", "input.txt")) == 8401
        end

        @testset "Part 02" begin
            @test Year2022.Day03.part02(@file("Day03", "test.txt")) == 70
            @test Year2022.Day03.part02(@file("Day03", "input.txt")) == 2641
        end
    end

    @testset "Day 04" begin
        @testset "Part 01" begin
            @test Year2022.Day04.part01(@file("Day04", "test.txt")) == 2
            @test Year2022.Day04.part01(@file("Day04", "input.txt")) == 576
        end

        @testset "Part 02" begin
            @test Year2022.Day04.part02(@file("Day04", "test.txt")) == 4
            @test Year2022.Day04.part02(@file("Day04", "input.txt")) == 905
        end
    end

    @testset "Day 05" begin
        @testset "Part 01" begin
            @test Year2022.Day05.part01(@file("Day05", "test.txt")) == "CMZ"
            @test Year2022.Day05.part01(@file("Day05", "input.txt")) == "CWMTGHBDW"
        end

        @testset "Part 02" begin
            @test Year2022.Day05.part02(@file("Day05", "test.txt")) == "MCD"
            @test Year2022.Day05.part02(@file("Day05", "input.txt")) == "SSCGWJCRB"
        end
    end

    @testset "Day 06" begin
        @testset "Part 01" begin
            @test Year2022.Day06.part01(@file("Day06", "test-1.txt")) == 7
            @test Year2022.Day06.part01(@file("Day06", "test-2.txt")) == 5
            @test Year2022.Day06.part01(@file("Day06", "test-3.txt")) == 6
            @test Year2022.Day06.part01(@file("Day06", "test-4.txt")) == 10
            @test Year2022.Day06.part01(@file("Day06", "test-5.txt")) == 11
            @test Year2022.Day06.part01(@file("Day06", "input.txt")) == 1100
        end

        @testset "Part 02" begin
            @test Year2022.Day06.part02(@file("Day06", "test-1.txt")) == 19
            @test Year2022.Day06.part02(@file("Day06", "test-2.txt")) == 23
            @test Year2022.Day06.part02(@file("Day06", "test-3.txt")) == 23
            @test Year2022.Day06.part02(@file("Day06", "test-4.txt")) == 29
            @test Year2022.Day06.part02(@file("Day06", "test-5.txt")) == 26
            @test Year2022.Day06.part02(@file("Day06", "input.txt")) == 2421
        end
    end

    @testset "Day 07" begin
        @testset "Part 01" begin
            @test Year2022.Day07.part01(@file("Day07", "test.txt")) == 95437
            @test Year2022.Day07.part01(@file("Day07", "input.txt")) == 1583951
        end

        @testset "Part 02" begin
            @test Year2022.Day07.part02(@file("Day07", "test.txt")) == 24933642
            @test Year2022.Day07.part02(@file("Day07", "input.txt")) == 214171
        end
    end

    @testset "Day 08" begin
        @testset "Part 01" begin
            @test Year2022.Day08.part01(@file("Day08", "test.txt")) == 21
            @test Year2022.Day08.part01(@file("Day08", "input.txt")) == 1807
        end

        @testset "Part 02" begin
            @test Year2022.Day08.part02(@file("Day08", "test.txt")) == 8
            @test Year2022.Day08.part02(@file("Day08", "input.txt")) == 480000
        end
    end

    @testset "Day 09" begin
        @testset "Part 01" begin
            @test Year2022.Day09.part01(@file("Day09", "test-1.txt")) == 13
            @test Year2022.Day09.part01(@file("Day09", "input.txt")) == 6081
        end

        @testset "Part 02" begin
            @test Year2022.Day09.part02(@file("Day09", "test-1.txt")) == 1
            @test Year2022.Day09.part02(@file("Day09", "test-2.txt")) == 36
            @test Year2022.Day09.part02(@file("Day09", "input.txt")) == 2487
        end
    end
end
