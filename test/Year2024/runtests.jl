@testset "Day 01" begin
    @testset "Part 01" begin
        @test Year2024.Day01.part01(@file("Day01", "test.txt")) == 11
        @test Year2024.Day01.part01(@file("Day01", "input.txt")) == 1882714
    end

    @testset "Part 02" begin
        @test Year2024.Day01.part02(@file("Day01", "test.txt")) == 31
        @test Year2024.Day01.part02(@file("Day01", "input.txt")) == 19437052
    end
end

@testset "Day 02" begin
    @testset "Part 01" begin
        @test Year2024.Day02.part01(@file("Day02", "test.txt")) == 2
        @test Year2024.Day02.part01(@file("Day02", "input.txt")) == 670
    end

    @testset "Part 02" begin
        @test Year2024.Day02.part02(@file("Day02", "test.txt")) == 4
        @test Year2024.Day02.part02(@file("Day02", "input.txt")) == 700
    end
end

@testset "Day 03" begin
    @testset "Part 01" begin
        @test Year2024.Day03.part01(@file("Day03", "test01.txt")) == 161
        @test Year2024.Day03.part01(@file("Day03", "test02.txt")) == 161
        @test Year2024.Day03.part01(@file("Day03", "input.txt")) == 189527826
    end

    @testset "Part 02" begin
        @test Year2024.Day03.part02(@file("Day03", "test01.txt")) == 161
        @test Year2024.Day03.part02(@file("Day03", "test02.txt")) == 48
        @test Year2024.Day03.part02(@file("Day03", "input.txt")) == 63013756
    end
end

@testset "Day 04" begin
    @testset "Part 01" begin
        @test Year2024.Day04.part01(@file("Day04", "test.txt")) == 18
        @test Year2024.Day04.part01(@file("Day04", "input.txt")) == 2464
    end

    @testset "Part 02" begin
        @test Year2024.Day04.part02(@file("Day04", "test.txt")) == 9
        @test Year2024.Day04.part02(@file("Day04", "input.txt")) == 1982
    end
end

@testset "Day 05" begin
    @testset "Part 01" begin
        @test Year2024.Day05.part01(@file("Day05", "test.txt")) == 143
        @test Year2024.Day05.part01(@file("Day05", "input.txt")) == 5639
    end

    @testset "Part 02" begin
        @test Year2024.Day05.part02(@file("Day05", "test.txt")) == 123
        @test Year2024.Day05.part02(@file("Day05", "input.txt")) == 5273
    end
end

@testset "Day 06" begin
    @testset "Part 01" begin
        @test Year2024.Day06.part01(@file("Day06", "test.txt")) == 41
        @test Year2024.Day06.part01(@file("Day06", "input.txt")) == 5564
    end

    @testset "Part 02" begin
        @test Year2024.Day06.part02(@file("Day06", "test.txt")) == 6
        @test Year2024.Day06.part02(@file("Day06", "input.txt")) == 1976
    end
end

@testset "Day 07" begin
    @testset "Part 01" begin
        @test Year2024.Day07.part01(@file("Day07", "test.txt")) == 3749
        @test Year2024.Day07.part01(@file("Day07", "input.txt")) == 975671981569
    end

    @testset "Part 02" begin
        @test Year2024.Day07.part02(@file("Day07", "test.txt")) == 11387
        @test Year2024.Day07.part02(@file("Day07", "input.txt")) == 223472064194845
    end
end

@testset "Day 08" begin
    @testset "Part 01" begin
        @test Year2024.Day08.part01(@file("Day08", "test.txt")) == 14
        @test Year2024.Day08.part01(@file("Day08", "input.txt")) == 396
    end

    @testset "Part 02" begin
        @test Year2024.Day08.part02(@file("Day08", "test.txt")) == 34
        @test Year2024.Day08.part02(@file("Day08", "input.txt")) == 1200
    end
end

@testset "Day 09" begin
    @testset "Part 01" begin
        @test Year2024.Day09.part01(@file("Day09", "test.txt")) == 1928
        @test Year2024.Day09.part01(@file("Day09", "input.txt")) == 6283404590840
    end

    @testset "Part 02" begin
        @test Year2024.Day09.part02(@file("Day09", "test.txt")) == 2858
        @test Year2024.Day09.part02(@file("Day09", "input.txt")) == 6304576012713
    end
end

@testset "Day 10" begin
    @testset "Part 01" begin
        @test Year2024.Day10.part01(@file("Day10", "test01.txt")) == 1
        @test Year2024.Day10.part01(@file("Day10", "test02.txt")) == 36
        @test Year2024.Day10.part01(@file("Day10", "input.txt")) == 746
    end

    @testset "Part 02" begin
        @test Year2024.Day10.part02(@file("Day10", "test01.txt")) == 16
        @test Year2024.Day10.part02(@file("Day10", "test02.txt")) == 81
        @test Year2024.Day10.part02(@file("Day10", "input.txt")) == 1541
    end
end

@testset "Day 11" begin
    @testset "Part 01" begin
        @test Year2024.Day11.part01(@file("Day11", "test.txt")) == 55312
        @test Year2024.Day11.part01(@file("Day11", "input.txt")) == 228668
    end

    @testset "Part 02" begin
        @test Year2024.Day11.part02(@file("Day11", "test.txt")) == 65601038650482
        @test Year2024.Day11.part02(@file("Day11", "input.txt")) == 270673834779359
    end
end

@testset "Day 12" begin
    @testset "Part 01" begin
        @test Year2024.Day12.part01(@file("Day12", "test01.txt")) == 140
        @test Year2024.Day12.part01(@file("Day12", "test02.txt")) == 772
        @test Year2024.Day12.part01(@file("Day12", "test03.txt")) == 1930
        @test Year2024.Day12.part01(@file("Day12", "test04.txt")) == 692
        @test Year2024.Day12.part01(@file("Day12", "test05.txt")) == 1184
        @test Year2024.Day12.part01(@file("Day12", "input.txt")) == 1400386
    end

    @testset "Part 02" begin
        @test Year2024.Day12.part02(@file("Day12", "test01.txt")) == 80
        @test Year2024.Day12.part02(@file("Day12", "test02.txt")) == 436
        @test Year2024.Day12.part02(@file("Day12", "test03.txt")) == 1206
        @test Year2024.Day12.part02(@file("Day12", "test04.txt")) == 236
        @test Year2024.Day12.part02(@file("Day12", "test05.txt")) == 368
        @test Year2024.Day12.part02(@file("Day12", "input.txt")) == 851994
    end
end

@testset "Day 13" begin
    @testset "Part 01" begin
        @test Year2024.Day13.part01(@file("Day13", "test.txt")) == 480
        @test Year2024.Day13.part01(@file("Day13", "input.txt")) == 31623
    end

    @testset "Part 02" begin
        @test Year2024.Day13.part02(@file("Day13", "test.txt")) == 875318608908
        @test Year2024.Day13.part02(@file("Day13", "input.txt")) == 93209116744825
    end
end

@testset "Day 14" begin
    @testset "Part 01" begin
        @test Year2024.Day14.part01(@file("Day14", "test.txt"), (11, 7)) == 12
        @test Year2024.Day14.part01(@file("Day14", "input.txt")) == 228690000
    end
end

@testset "Day 15" begin
    @testset "Part 01" begin
        @test Year2024.Day15.part01(@file("Day15", "test01.txt")) == 2028
        @test Year2024.Day15.part01(@file("Day15", "test02.txt")) == 10092
        @test Year2024.Day15.part01(@file("Day15", "test03.txt")) == 908
        @test Year2024.Day15.part01(@file("Day15", "input.txt")) == 1436690
    end

    @testset "Part 02" begin
        @test Year2024.Day15.part02(@file("Day15", "test01.txt")) == 1751
        @test Year2024.Day15.part02(@file("Day15", "test02.txt")) == 9021
        @test Year2024.Day15.part02(@file("Day15", "test03.txt")) == 618
        @test Year2024.Day15.part02(@file("Day15", "input.txt")) == 1482350
    end
end

@testset "Day 16" begin
    @testset "Part 01" begin
        @test Year2024.Day16.part01(@file("Day16", "test01.txt")) == 7036
        @test Year2024.Day16.part01(@file("Day16", "test02.txt")) == 11048
        @test Year2024.Day16.part01(@file("Day16", "input.txt")) == 108504
    end
end

@testset "Day 18" begin
    @testset "Part 01" begin
        @test Year2024.Day18.part01(@file("Day18", "test.txt"), 6, 12) == 22
        @test Year2024.Day18.part01(@file("Day18", "input.txt")) == 306
    end

    @testset "Part 02" begin
        @test Year2024.Day18.part02(@file("Day18", "test.txt"), 6, 12) == (6, 1)
        @test Year2024.Day18.part02(@file("Day18", "input.txt")) == (38, 63)
    end
end

@testset "Day 19" begin
    @testset "Part 01" begin
        @test Year2024.Day19.part01(@file("Day19", "test.txt")) == 6
        @test Year2024.Day19.part01(@file("Day19", "input.txt")) == 242
    end

    @testset "Part 02" begin
        @test Year2024.Day19.part02(@file("Day19", "test.txt")) == 16
        @test Year2024.Day19.part02(@file("Day19", "input.txt")) == 595975512785325
    end
end
