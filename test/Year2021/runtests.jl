@testset "Year 2021" begin
    @testset "Day 01" begin
        @testset "Part 01" begin
            @test Year2021.Day01.part01(@file("Day01", "test.txt")) == 7
            @test Year2021.Day01.part01(@file("Day01", "input.txt")) == 1288
        end

        @testset "Part 02" begin
            @test Year2021.Day01.part02(@file("Day01", "test.txt")) == 5
            @test Year2021.Day01.part02(@file("Day01", "input.txt")) == 1311
        end
    end

    @testset "Day 02" begin
        @testset "Part 01" begin
            @test Year2021.Day02.part01(@file("Day02", "test.txt")) == 150
            @test Year2021.Day02.part01(@file("Day02", "input.txt")) == 1989265
        end

        @testset "Part 02" begin
            @test Year2021.Day02.part02(@file("Day02", "test.txt")) == 900
            @test Year2021.Day02.part02(@file("Day02", "input.txt")) == 2089174012
        end
    end

    @testset "Day 03" begin
        @testset "Part 01" begin
            @test Year2021.Day03.part01(@file("Day03", "test.txt")) == 198
            @test Year2021.Day03.part01(@file("Day03", "input.txt")) == 2724524
        end

        @testset "Part 02" begin
            @test Year2021.Day03.part02(@file("Day03", "test.txt")) == 230
            @test Year2021.Day03.part02(@file("Day03", "input.txt")) == 2775870
        end
    end

    @testset "Day 04" begin
        @testset "Part 01" begin
            @test Year2021.Day04.part01(@file("Day04", "test.txt")) == 4512
            @test Year2021.Day04.part01(@file("Day04", "input.txt")) == 71708
        end

        @testset "Part 02" begin
            @test Year2021.Day04.part02(@file("Day04", "test.txt")) == 1924
            @test Year2021.Day04.part02(@file("Day04", "input.txt")) == 34726
        end
    end

    @testset "Day 05" begin
        @testset "Part 01" begin
            @test Year2021.Day05.part01(@file("Day05", "test.txt")) == 5
            @test Year2021.Day05.part01(@file("Day05", "input.txt")) == 7318
        end

        @testset "Part 02" begin
            @test Year2021.Day05.part02(@file("Day05", "test.txt")) == 12
            @test Year2021.Day05.part02(@file("Day05", "input.txt")) == 19939
        end
    end

    @testset "Day 06" begin
        @testset "Part 01" begin
            @test Year2021.Day06.part01(@file("Day06", "test.txt")) == 5934
            @test Year2021.Day06.part01(@file("Day06", "input.txt")) == 350149
        end

        @testset "Part 02" begin
            @test Year2021.Day06.part02(@file("Day06", "test.txt")) == 26984457539
            @test Year2021.Day06.part02(@file("Day06", "input.txt")) == 1590327954513
        end
    end

    @testset "Day 07" begin
        @testset "Part 01" begin
            @test Year2021.Day07.part01(@file("Day07", "test.txt")) == 37
            @test Year2021.Day07.part01(@file("Day07", "input.txt")) == 336120
        end

        @testset "Part 02" begin
            @test Year2021.Day07.part02(@file("Day07", "test.txt")) == 168
            @test Year2021.Day07.part02(@file("Day07", "input.txt")) == 96864235
        end
    end

    @testset "Day 08" begin
        @testset "Part 01" begin
            @test Year2021.Day08.part01(@file("Day08", "test.txt")) == 26
            @test Year2021.Day08.part01(@file("Day08", "input.txt")) == 383
        end

        @testset "Part 02" begin
            @test Year2021.Day08.part02(@file("Day08", "test.txt")) == 61229
            @test Year2021.Day08.part02(@file("Day08", "input.txt")) == 998900
        end
    end

    @testset "Day 09" begin
        @testset "Part 01" begin
            @test Year2021.Day09.part01(@file("Day09", "test.txt")) == 15
            @test Year2021.Day09.part01(@file("Day09", "input.txt")) == 439
        end

        @testset "Part 02" begin
            @test Year2021.Day09.part02(@file("Day09", "test.txt")) == 1134
            @test Year2021.Day09.part02(@file("Day09", "input.txt")) == 900900
        end
    end

    @testset "Day 10" begin
        @testset "Part 01" begin
            @test Year2021.Day10.part01(@file("Day10", "test.txt")) == 26397
            @test Year2021.Day10.part01(@file("Day10", "input.txt")) == 344193
        end

        @testset "Part 02" begin
            @test Year2021.Day10.part02(@file("Day10", "test.txt")) == 288957
            @test Year2021.Day10.part02(@file("Day10", "input.txt")) == 3241238967
        end
    end

    @testset "Day 11" begin
        @testset "Part 01" begin
            @test Year2021.Day11.part01(@file("Day11", "test.txt")) == 1656
            @test Year2021.Day11.part01(@file("Day11", "input.txt")) == 1591
        end

        @testset "Part 02" begin
            @test Year2021.Day11.part02(@file("Day11", "test.txt")) == 195
            @test Year2021.Day11.part02(@file("Day11", "input.txt")) == 314
        end
    end

    @testset "Day 12" begin
        @testset "Part 01" begin
            @test Year2021.Day12.part01(@file("Day12", "test-01.txt")) == 10
            @test Year2021.Day12.part01(@file("Day12", "test-02.txt")) == 19
            @test Year2021.Day12.part01(@file("Day12", "test-03.txt")) == 226
            @test Year2021.Day12.part01(@file("Day12", "input.txt")) == 4659
        end

        @testset "Part 02" begin
            @test Year2021.Day12.part02(@file("Day12", "test-01.txt")) == 36
            @test Year2021.Day12.part02(@file("Day12", "test-02.txt")) == 103
            @test Year2021.Day12.part02(@file("Day12", "test-03.txt")) == 3509
            @test Year2021.Day12.part02(@file("Day12", "input.txt")) == 148962
        end
    end

    @testset "Day 13" begin
        @testset "Part 01" begin
            @test Year2021.Day13.part01(@file("Day13", "test.txt")) == 17
            @test Year2021.Day13.part01(@file("Day13", "input.txt")) == 618
        end

        @testset "Part 02" begin
            @test Year2021.Day13.part02(@file("Day13", "test.txt")) == "O"
            @test Year2021.Day13.part02(@file("Day13", "input.txt")) == "ALREKFKU"
        end
    end

    @testset "Day 14" begin
        @testset "Part 01" begin
            @test Year2021.Day14.part01(@file("Day14", "test.txt")) == 1588
            @test Year2021.Day14.part01(@file("Day14", "input.txt")) == 2194
        end

        @testset "Part 02" begin
            @test Year2021.Day14.part02(@file("Day14", "test.txt")) == 2188189693529
            @test Year2021.Day14.part02(@file("Day14", "input.txt")) == 2360298895777
        end
    end

    @testset "Day 15" begin
        @testset "Part 01" begin
            @test Year2021.Day15.part01(@file("Day15", "test.txt")) == 40
            @test Year2021.Day15.part01(@file("Day15", "input.txt")) == 790
        end

        @testset "Part 02" begin
            @test Year2021.Day15.part02(@file("Day15", "test.txt")) == 315
            @test Year2021.Day15.part02(@file("Day15", "input.txt")) == 2998
        end
    end

    @testset "Day 16" begin
        @testset "Part 01" begin
            @test Year2021.Day16.part01(@file("Day16", "test-01-01.txt")) == 16
            @test Year2021.Day16.part01(@file("Day16", "test-01-02.txt")) == 12
            @test Year2021.Day16.part01(@file("Day16", "test-01-03.txt")) == 23
            @test Year2021.Day16.part01(@file("Day16", "test-01-04.txt")) == 31
            @test Year2021.Day16.part01(@file("Day16", "input.txt")) == 847
        end

        @testset "Part 02" begin
            @test Year2021.Day16.part02(@file("Day16", "test-02-01.txt")) == 3
            @test Year2021.Day16.part02(@file("Day16", "test-02-02.txt")) == 54
            @test Year2021.Day16.part02(@file("Day16", "test-02-03.txt")) == 7
            @test Year2021.Day16.part02(@file("Day16", "test-02-04.txt")) == 9
            @test Year2021.Day16.part02(@file("Day16", "test-02-05.txt")) == 1
            @test Year2021.Day16.part02(@file("Day16", "test-02-06.txt")) == 0
            @test Year2021.Day16.part02(@file("Day16", "test-02-07.txt")) == 0
            @test Year2021.Day16.part02(@file("Day16", "test-02-08.txt")) == 1
            @test Year2021.Day16.part02(@file("Day16", "input.txt")) == 333794664059
        end
    end
end
